package com.richardoruna.app.adminchurch.presenter.asistencia;

import com.richardoruna.app.adminchurch.model.entity.AsistenciaCelula;

import java.util.List;

/**
 * Created by Ricoru on 18/09/17.
 */

public interface IHistorialAsistenciaPresenter {

    void getAsistencias();

    void data(List<AsistenciaCelula> miembroList);
    void error(Throwable t);

}
