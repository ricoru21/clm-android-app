package com.richardoruna.app.adminchurch.model.dao;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ricoru on 3/09/17.
 */

public class MemberDAO {

    private static FirebaseDatabase mDatabase;
    private ValueEventListener valueEventListener;
    private List<Miembro> miembroList = new ArrayList<Miembro>();

    public MemberDAO(){
        mDatabase = FirebaseDatabase.getInstance();
    }

    public void save(Miembro miembro, final IComplete<Void> iComplete){
        DatabaseReference mReferences=null;
        mReferences = mDatabase.getReference(FirebaseConstant.TAG_MIEMBRO_RED)
                .child(String.valueOf(miembro.red.charAt(miembro.red.length()-1)))
                .child(FirebaseConstant.TAG_MIEMBRO);
        mReferences.push().setValue(miembro.toMap(), new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError==null){
                    iComplete.success(null);
                }else {
                    iComplete.failure(databaseError.toException());
                }
            }
        });
    }

    public void update(Miembro miembro, final IComplete<Void> iComplete){
        DatabaseReference mReferences=null;
        mReferences = mDatabase.getReference(FirebaseConstant.TAG_MIEMBRO_RED)
                .child(String.valueOf(miembro.red.charAt(miembro.red.length()-1)))
                .child(FirebaseConstant.TAG_MIEMBRO)
                .child(miembro.key);
        mReferences.setValue(miembro.toMap(), new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError==null){
                    iComplete.success(null);
                }else {
                    iComplete.failure(databaseError.toException());
                }
            }
        });
    }

    public void remove(Miembro miembro, final IComplete<Void> iComplete){
        DatabaseReference mReferences=null;
        mReferences = mDatabase.getReference(FirebaseConstant.TAG_MIEMBRO_RED)
                .child(String.valueOf(miembro.red.charAt(miembro.red.length()-1)))
                .child(FirebaseConstant.TAG_MIEMBRO)
                .child(miembro.key);
        mReferences.removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError==null){
                    iComplete.success(null);
                }else {
                    iComplete.failure(databaseError.toException());
                }
            }
        });
    }

    public void getMembers(String red,final IComplete<List<Miembro>> iComplete ) {
        mDatabase.getReference(FirebaseConstant.TAG_MIEMBRO_RED)
                .child(String.valueOf(red.charAt(red.length()-1)))
                .child(FirebaseConstant.TAG_MIEMBRO)
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                miembroList = new ArrayList<Miembro>();
                if(dataSnapshot.exists()){
                    try{
                        for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                            Miembro miembro = snapshot.getValue(Miembro.class);
                            miembro.key =snapshot.getKey();
                            miembroList.add(miembro);
                        }
                    }catch (Exception e){
                        iComplete.failure(e);
                    }
                }
                iComplete.success(miembroList);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                iComplete.failure(databaseError.toException());
            }
        });
    }

}
