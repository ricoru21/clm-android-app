package com.richardoruna.app.adminchurch.presenter.asistencia;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;

import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.AsistenciaDominicalModel;
import com.richardoruna.app.adminchurch.model.IAsistenciaDominicalModel;
import com.richardoruna.app.adminchurch.model.entity.AsistenciaDominical;
import com.richardoruna.app.adminchurch.model.entity.Turno;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.ui.MvpApp;
import com.richardoruna.app.adminchurch.ui.activity.RegistroAsistenciaDominicalActivity;
import com.richardoruna.app.adminchurch.util.DateUtil;
import com.richardoruna.app.adminchurch.util.Preferences;
import com.richardoruna.app.adminchurch.view.IAsistenciaDominicalView;

import java.util.List;

/**
 * Created by Ricoru on 22/09/17.
 */

public class AsistenciaDominicalPresenter implements IAsistenciaDominicalPresenter {

    IAsistenciaDominicalView iAsistenciaDominicalView;
    IAsistenciaDominicalModel iAsistenciaDominicalModel;

    List<AsistenciaDominical> asistenciaDominicalList;
    SharedPreferences utilPreferences;
    Usuario usuario;

    public AsistenciaDominicalPresenter(IAsistenciaDominicalView iAsistenciaDominicalView) {
        this.iAsistenciaDominicalView = iAsistenciaDominicalView;
        this.iAsistenciaDominicalModel = new AsistenciaDominicalModel(this);
        utilPreferences = Preferences.getDefaultPreferences(MvpApp.getContext());
    }

    @Override
    public void getAsistencias() {
        iAsistenciaDominicalModel.getAsistencias();
    }

    @Override
    public void data(List<AsistenciaDominical> asistenciaDominicalList) {
        this.asistenciaDominicalList = asistenciaDominicalList;
        iAsistenciaDominicalView.setModel(asistenciaDominicalList);
    }

    @Override
    public void getTurnos() {
        iAsistenciaDominicalModel.getTurnos();
    }

    @Override
    public void dataTurnos(List<Turno> turnoList) {
        iAsistenciaDominicalView.setOptionSpinner(turnoList);
    }

    @Override
    public void error(Throwable e) {
        iAsistenciaDominicalView.hideProgress();
        if(e!=null) {
            e.printStackTrace();
            iAsistenciaDominicalView.message(e.getMessage());
        } else iAsistenciaDominicalView.message(R.string.error_internal);
    }

    @Override
    public void save(Turno turno, String fecha_asistencia, String fecha_asistencia_format, String nh_adultos,
                     String nh_jovenes, String nh_ninios, String nm_adultas, String nm_jovenes, String n_nuevos){

        if(turno==null){
            iAsistenciaDominicalView.message("Es necesario elegir un Turno para registrar asistencia");
            return;
        }
        if(TextUtils.isEmpty(fecha_asistencia)){
            iAsistenciaDominicalView.message("El campo fecha es requerido");
            return;
        }
        if(TextUtils.isEmpty(nh_adultos)){
            nh_adultos = "0";
        }
        if(TextUtils.isEmpty(nh_jovenes)){
            nh_jovenes = "0";
        }
        if(TextUtils.isEmpty(nh_ninios)){
            nh_ninios = "0";
        }
        if(TextUtils.isEmpty(nm_adultas)){
            nm_adultas = "0";
        }
        if(TextUtils.isEmpty(nm_jovenes)){
            nh_jovenes = "0";
        }
        if(TextUtils.isEmpty(n_nuevos)){
            n_nuevos = "0";
        }

        AsistenciaDominical asistenciaDominical = new AsistenciaDominical();
        asistenciaDominical.fecha = DateUtil.formatStringToDate(fecha_asistencia);
        asistenciaDominical.fechaMostrar = fecha_asistencia_format;
        asistenciaDominical.nombre_turno = turno.nombre;
        asistenciaDominical.horario_turno = turno.horario;
        asistenciaDominical.key_turno = turno.key;
        asistenciaDominical.nh_adultos = Integer.parseInt(nh_adultos);
        asistenciaDominical.nh_jovenes = Integer.parseInt(nh_jovenes);
        asistenciaDominical.nh_ninios = Integer.parseInt(nh_ninios);
        asistenciaDominical.nm_adultas = Integer.parseInt(nm_adultas);
        asistenciaDominical.nm_jovenes = Integer.parseInt(nm_jovenes);
        asistenciaDominical.n_nuevos = Integer.parseInt(n_nuevos);
        iAsistenciaDominicalView.showProgress();
        iAsistenciaDominicalModel.save(asistenciaDominical);
    }

    @Override
    public void success(Constant.typeAction typeAction) {
        iAsistenciaDominicalView.hideProgress();
        if(typeAction.equals(Constant.typeAction.SAVE)){
            iAsistenciaDominicalView.message(R.string.success_reg_asistencia_dominical);
            iAsistenciaDominicalView.finish();
        }
    }

    @Override
    public void irRegistroAsistencia(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(RegistroAsistenciaDominicalActivity.ARGUMENT_EXTRA_SAVE, true);
        RegistroAsistenciaDominicalActivity.activityFrom(context, bundle);
    }

    @Override
    public void irEditarAsistencia(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(RegistroAsistenciaDominicalActivity.ARGUMENT_EXTRA_SAVE, false);
        RegistroAsistenciaDominicalActivity.activityFrom(context, bundle);
    }

    @Override
    public void clearPreferences() {
        Preferences.storeStringPreference(
                utilPreferences, Preferences.PREFERENCES_USER_UID, "");
        Preferences.storeStringPreference(
                utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY, "");
    }

    @Override
    public void changePrerfenceRol(String rol) {

    }

    /*@Override
    public void showRoles() {
        iAsistenciaDominicalView.showDialogProfile(usuario.roles);
    }

    @Override
    public void changePrerfenceProfile(int index_item) {
        Preferences.storeStringPreference(
                utilPreferences, Preferences.PREFERENCES_USER_ROL_SELECT,
                usuario.roles.get(index_item).toString());
        //una vez q cambia de index, hay q cambiar de pantalla
    }*/

}
