package com.richardoruna.app.adminchurch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.entity.AsistenciaCelula;
import com.richardoruna.app.adminchurch.util.DateUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ricoru on 18/09/17.
 */

public class HistorialAsistenciaAdapter extends RecyclerView.Adapter<HistorialAsistenciaAdapter.HistorialAsistenciaHolder>{

    private List<AsistenciaCelula> mLista;
    //private IRedCelulaView iRedCelulaView;
    private View view;

    public HistorialAsistenciaAdapter(List<AsistenciaCelula> mLista) {
        this.mLista=mLista;
        //this.iRedCelulaView = iRedCelulaView;
        // TODO falta agregar la interfaz para realizar la eliminación de las asistencias registradas.
    }

    @Override
    public HistorialAsistenciaHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View viewSender = inflater.inflate(R.layout.row_item_historial_asistencia, viewGroup, false);
        return new HistorialAsistenciaHolder(viewSender);
    }

    @Override
    public void onBindViewHolder(final HistorialAsistenciaHolder holder, int position) {
        final AsistenciaCelula asistenciaCelula = mLista.get(position);
        holder.txt_fecha.setText(DateUtil.LongToFechaHora(asistenciaCelula.fechaAsistencia, "dd/MM/yyyy"));
        holder.txt_nombre.setText(asistenciaCelula.celula.nombre+" - "+ asistenciaCelula.celula.horario);
        holder.txt_nro_asistencia.setText(String.format(view.getResources()
                .getString(R.string.lbl_nro_asistencia), asistenciaCelula.nroAsistido));
        holder.txt_nro_miembros.setText(
                String.format(view.getResources()
                        .getString(R.string.lbl_nro_miembros), asistenciaCelula.nroMiembros));

        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO falta enviar el objeto para eliminar de la lista de historial de asistencias
            }
        });
    }

    public List<AsistenciaCelula> getList(){
        return mLista;
    }

    @Override
    public int getItemCount() {
        return mLista.size();
    }


    public class HistorialAsistenciaHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.content) LinearLayout content;
        @BindView(R.id.txt_nombre) TextView txt_nombre;
        @BindView(R.id.txt_fecha) TextView txt_fecha;
        @BindView(R.id.txt_nro_miembros) TextView txt_nro_miembros;
        @BindView(R.id.txt_nro_asistencia) TextView txt_nro_asistencia;

        public HistorialAsistenciaHolder(View itemView) {
            super(itemView);
            view = itemView;
            ButterKnife.bind(this,itemView);
        }

    }

}
