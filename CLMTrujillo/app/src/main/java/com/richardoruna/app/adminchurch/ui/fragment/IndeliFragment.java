package com.richardoruna.app.adminchurch.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.presenter.IIndeliPresenter;
import com.richardoruna.app.adminchurch.presenter.IndeliPresenter;
import com.richardoruna.app.adminchurch.util.MediaUtil;
import com.richardoruna.app.adminchurch.view.IIndeliView;

import butterknife.ButterKnife;
import butterknife.Unbinder;


public class IndeliFragment extends Fragment implements IIndeliView {

    private Unbinder unbinder;
    private MediaUtil mediaUtil;
    private IIndeliPresenter iIndeliPresenter;


    public IndeliFragment() {
        // Required empty public constructor
    }

    public static IndeliFragment newInstance() {
        IndeliFragment fragment = new IndeliFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        iIndeliPresenter = new IndeliPresenter(this);
        mediaUtil = new MediaUtil(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_indeli, container, false);
        unbinder = ButterKnife.bind(this, view);
        //aquí hacer el proceso de seteo
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void message(@StringRes int msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void message(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
