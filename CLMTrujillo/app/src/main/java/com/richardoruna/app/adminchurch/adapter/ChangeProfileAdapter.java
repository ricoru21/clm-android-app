package com.richardoruna.app.adminchurch.adapter;

import android.app.Activity;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.ui.MvpApp;
import com.richardoruna.app.adminchurch.util.Preferences;

import java.util.ArrayList;

/**
 * Created by Ricoru on 25/09/17.
 */

public class ChangeProfileAdapter extends BaseAdapter {

    private Activity context;
    private ArrayList<String> mData;
    private SharedPreferences utilPreferences;
    private String rol_seleccionado ="";

    public ChangeProfileAdapter(Activity context, ArrayList<String> items)
    {
        this.mData = items;
        this.context = context;
        this.utilPreferences = Preferences.getDefaultPreferences(MvpApp.getContext());
        rol_seleccionado = Preferences.findStringPreference(
                utilPreferences, Preferences.PREFERENCES_USER_ROL_SELECT, "");
    }

    @Override
    public String getItem(int position) {
        return mData.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View result;
        ViewHolder holder;
        if (convertView == null) {
            result = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_change_profile, parent, false);
            holder = new ViewHolder();
            holder.txt_other_usuario = (TextView) result.findViewById(R.id.txt_other_usuario);
            holder.txt_tipo = (TextView) result.findViewById(R.id.txt_tipo);
            result.setTag(holder);
        } else {
            result = convertView;
            holder = (ViewHolder) convertView.getTag();
        }

        String item = getItem(position);
        if(item!=null){
            holder.txt_other_usuario.setText(Constant.roles_name.get(item));
            if(rol_seleccionado.equalsIgnoreCase(item)){
                holder.txt_tipo.setVisibility(View.VISIBLE);
            }
        }
        return result;
    }

    class ViewHolder
    {
        TextView txt_other_usuario;
        TextView txt_tipo;
    }

}
