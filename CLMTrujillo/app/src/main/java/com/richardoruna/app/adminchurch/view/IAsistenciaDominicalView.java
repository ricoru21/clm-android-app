package com.richardoruna.app.adminchurch.view;

import com.richardoruna.app.adminchurch.model.entity.AsistenciaDominical;
import com.richardoruna.app.adminchurch.model.entity.Turno;

import java.util.List;

/**
 * Created by Ricoru on 22/09/17.
 */

public interface IAsistenciaDominicalView extends IBaseView {

    void setModel(List<AsistenciaDominical> asistenciaDominicalList);

    void setOptionSpinner(List<Turno> turnoList);
    void showProgress();
    void hideProgress();
    void finish();

}
