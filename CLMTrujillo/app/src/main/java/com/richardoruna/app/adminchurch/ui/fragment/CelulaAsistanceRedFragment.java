package com.richardoruna.app.adminchurch.ui.fragment;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.Spinner;

import com.afollestad.materialdialogs.MaterialDialog;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.adapter.CelulaAsistenciaMemberAdapter;
import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.presenter.asistencia.CelulaAsistenciaRedPresenter;
import com.richardoruna.app.adminchurch.presenter.asistencia.ICelulaAsistenciaRedPresenter;
import com.richardoruna.app.adminchurch.util.StringUtil;
import com.richardoruna.app.adminchurch.view.ICelulaAsistenciaRedView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;

public class CelulaAsistanceRedFragment extends Fragment implements ICelulaAsistenciaRedView,
        CalendarDatePickerDialogFragment.OnDateSetListener {

    @BindView(R.id.rcv_lista)
    RecyclerView rcv_lista;
    @BindView(R.id.content_frame)
    FrameLayout content_frame;
    @BindView(R.id.sp_celula)
    Spinner sp_celula;
    @BindView(R.id.edit_fecha)
    AppCompatEditText edit_fecha;
    @BindView(R.id.chk_select_all)
    CheckBox chk_select_all;

    Unbinder unbinder;
    Calendar calendar;
    Menu menu_temp;
    StringUtil stringUtil;
    ICelulaAsistenciaRedPresenter iCelulaAsistenciaRedPresenter;
    CelulaAsistenciaMemberAdapter adapter;
    List<Miembro> listMiembro, listMiembro_tmp;
    MaterialDialog progressDialog;

    private boolean flag_all=false;
    private boolean flag_one_selected=false;
    private int year=0 , month=0, day=0;

    public CelulaAsistanceRedFragment() {
        // Required empty public constructor
    }

    public static CelulaAsistanceRedFragment newInstance() {
        CelulaAsistanceRedFragment fragment = new CelulaAsistanceRedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            //Restore the fragment's state here
            Type listType = new TypeToken<ArrayList<Miembro>>(){}.getType();
            listMiembro = new Gson().fromJson(savedInstanceState.getString("lst_miembros"), listType);
            flag_one_selected = savedInstanceState.getBoolean("flag_one_selected");
            flag_all = savedInstanceState.getBoolean("flag_all");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu_temp = menu;
        menu.findItem(R.id.action_done).setVisible(false);
        menu.findItem(R.id.action_clear).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_done) {
            saveAsistencia();
            return true;
        }
        if(id == R.id.action_clear){
            clearData();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void init(){
        stringUtil = new StringUtil();
        calendar = Calendar.getInstance();
        iCelulaAsistenciaRedPresenter = new CelulaAsistenciaRedPresenter(this);
        listMiembro_tmp = new ArrayList<>();

        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH)+1;
        year = calendar.get(Calendar.YEAR);
        createProgressDialog();

        try{
            edit_fecha.setText(day+ "/"+stringUtil.number2Digits(month)+"/"+year);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        iCelulaAsistenciaRedPresenter.getCelulas();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_celula_asistance, container, false);
        unbinder = ButterKnife.bind(this,view);
        init();
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        unbinder.unbind();
    }

    @OnClick(R.id.edit_fecha)
    public void showDatetimePicker(){
        CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(this)
                .setThemeLight()
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setPreselectedDate(year, month-1, day)
                .setDoneText("Aceptar")
                .setCancelText("Cancelar");
        cdp.show(getFragmentManager(), null);
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String dateFormat= stringUtil.number2Digits(dayOfMonth)+"/";
        dateFormat+= stringUtil.number2Digits(monthOfYear+1)+"/";
        dateFormat+= year;

        this.year= year;
        this.month= monthOfYear+1;
        this.day= dayOfMonth;

        edit_fecha.setText(dateFormat);
    }

    @Override
    public void dataOptionSpinner(List<Celula> celulaList) {
        if (celulaList != null) {
            ArrayAdapter<List<Celula>> dataAdapter_tipo = new ArrayAdapter(getActivity(),
                    android.R.layout.simple_spinner_item, celulaList);
            /*{
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);
                    //((TextView) v).setTypeface(font);//Typeface for normal view
                    ((TextView) v).setTextSize(14f);
                    //((TextView) v).setTextColor(getResources().getColor(R.color.colorWhite));
                    AbsListView.LayoutParams lp = //new LinearLayout.LayoutParams(v.getLayoutParams());
                            new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.MATCH_PARENT);
                    lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                    lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                    v.setLayoutParams(lp);
                    v.setPadding(15,10,10,10);
                    return v;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    //((TextView) v).setTypeface(font);//Typeface for normal view
                    ((TextView) v).setTextSize(14f);
                    AbsListView.LayoutParams lp = //new LinearLayout.LayoutParams(v.getLayoutParams());
                            new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.MATCH_PARENT);
                    lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                    lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                    v.setLayoutParams(lp);
                    v.setPadding(15,10,10,10);
                    return v;
                }
            };*/
            dataAdapter_tipo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_celula.setAdapter(dataAdapter_tipo);
        }

    }

    @OnItemSelected(R.id.sp_celula)
    public void seleccionarCelula(AdapterView<?> adapterView, View view, int i, long l){
        Celula celula = (Celula)sp_celula.getItemAtPosition(i);
        if(celula!=null){
            setModel(celula.miembroList);
        }
    }

    public void setModel(List<Miembro> model){
        try {
            listMiembro = model;
            if (rcv_lista != null) {
                adapter = new CelulaAsistenciaMemberAdapter(model, this);
                rcv_lista.setAdapter(adapter);
                rcv_lista.setLayoutManager(new LinearLayoutManager(getActivity()));
                rcv_lista.setHasFixedSize(true);
                rcv_lista.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(!flag_one_selected){
                pintarTodos();
            }else{
                pintarSoloMarcados();
            }
        }
    }

    @Override
    public void onItemSeleccionado(Miembro miembro, int position) {
        // voy a seleccionar el elmento solo para cheaquearlo nada mas.
        adapter.getList().set(position, miembro);
        listMiembro.set(position, miembro);
        //verificar si ya todos estan marcados
        if(verificarMarcacionAll()){
            flag_one_selected = false;
            flag_all = true;
        }else{
            flag_all = false;
            flag_one_selected = true;
        }
        //Log.i("INFO","flag_all "+flag_all);
        //Log.i("INFO","flag_one_selected "+flag_one_selected);
    }

    @OnCheckedChanged(R.id.chk_select_all)
    public void chkAll(CompoundButton compoundButton, boolean b){
        flag_all = b;
        if(flag_all) flag_one_selected = false;
        else flag_one_selected = true;
        pintarTodos();
    }

    private void pintarTodos(){
        if(adapter!=null){
            if(adapter.getList()!=null){
                for(int i=0; i< adapter.getList().size(); i++){
                    Miembro miembro = adapter.getList().get(i);
                    miembro.asistio = flag_all;
                    adapter.getList().set(i, miembro);
                    listMiembro.set(i, miembro);
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void pintarSoloMarcados(){
        if(adapter!=null){
            if(listMiembro_tmp.size()==0){
                listMiembro_tmp = listMiembro;
            }else{
                listMiembro = listMiembro_tmp;
            }
            if(listMiembro!=null){
                for(int i=0; i< listMiembro.size(); i++){
                    Miembro miembro = adapter.getList().get(i);
                    miembro.asistio = listMiembro.get(i).asistio;
                    adapter.getList().set(i, miembro);
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    private boolean verificarMarcacionAll(){
        if(listMiembro.size()>0){
            menu_temp.findItem(R.id.action_clear).setVisible(true);
            menu_temp.findItem(R.id.action_done).setVisible(true);
        }else{
            menu_temp.findItem(R.id.action_clear).setVisible(false);
            menu_temp.findItem(R.id.action_done).setVisible(false);
        }
        for(int i=0; i< listMiembro.size(); i++){
            if(!listMiembro.get(i).asistio) return false;
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(listMiembro!=null) setModel(listMiembro);
    }

    @Override
    public void message(@StringRes int msj) {
        Snackbar.make(content_frame, msj, Snackbar.LENGTH_SHORT)
                .setAction("Action", null)
                .show();
    }

    @Override
    public void message(String msj) {
        Snackbar.make(content_frame, msj, Snackbar.LENGTH_SHORT)
                .setAction("Action", null)
                .show();
    }

    @Override
    public void clearData() {
        flag_all = false;
        edit_fecha.setText("");
        sp_celula.setSelection(0,true);
        chk_select_all.setChecked(flag_all);
        menu_temp.findItem(R.id.action_done).setVisible(false);
        menu_temp.findItem(R.id.action_clear).setVisible(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("flag_all",flag_all);
        outState.putBoolean("flag_one_selected", flag_one_selected);
        outState.putString("lst_miembros", new Gson().toJson(listMiembro));
    }

    private void createProgressDialog(){
        progressDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.celula_asistencia_title_dialog)
                .content(R.string.all_msj_dialog)
                .progress(true, 5000)
                .widgetColorRes(R.color.colorD929)
                .canceledOnTouchOutside(false)
                .cancelable(false)
                .build();
    }

    private void saveAsistencia(){
        if(edit_fecha.getText().toString().equalsIgnoreCase("")){
            message("Es necesario elegir una fecha de asistencia");
            return;
        }
        if(sp_celula.getSelectedItem()==null){
            message("No existe célula a registrar la asistencia");
            return;
        }
        String fechaHoy = year+"-"+month+"-"+day;
        Celula celula = (Celula) sp_celula.getSelectedItem();
        celula.miembroList = listMiembro;
        iCelulaAsistenciaRedPresenter.save(fechaHoy, celula);
    }

    @Override
    public void showProgress() { progressDialog.show();}

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

}
