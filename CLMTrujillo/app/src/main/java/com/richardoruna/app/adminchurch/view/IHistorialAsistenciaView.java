package com.richardoruna.app.adminchurch.view;

import com.richardoruna.app.adminchurch.model.entity.AsistenciaCelula;

import java.util.List;

/**
 * Created by Ricoru on 18/09/17.
 */

public interface IHistorialAsistenciaView extends IBaseView {
    void setModel(List<AsistenciaCelula> data);
}
