package com.richardoruna.app.adminchurch.presenter;

/**
 * Created by Ricoru on 25/09/17.
 */

public interface IMainPresenter extends IPreferencePresenter {

    void showDialogProfile();

}
