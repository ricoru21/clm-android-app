package com.richardoruna.app.adminchurch.model.dao;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.model.entity.AsistenciaCelula;
import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ricoru on 3/09/17.
 */

public class CelulaDAO {

    private static FirebaseDatabase mDatabase;
    private ValueEventListener valueEventListener;
    private List<Celula> celulaList = new ArrayList<Celula>();
    private List<AsistenciaCelula> asistenciaCelulaList = new ArrayList<>();

    public CelulaDAO(){
        mDatabase = FirebaseDatabase.getInstance();
    }

    public void save(Celula celula, final IComplete<Void> iComplete){
        DatabaseReference mReferences=null;
        mReferences = mDatabase.getReference(FirebaseConstant.TAG_MIEMBRO_RED)
                .child(String.valueOf(celula.red.charAt(celula.red.length()-1)))
                .child(FirebaseConstant.TAG_CELULA);
        mReferences.push().setValue(celula, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError==null){
                    iComplete.success(null);
                }else {
                    iComplete.failure(databaseError.toException());
                }
            }
        });
    }

    public void update(Celula celula, final IComplete<Void> iComplete){
        DatabaseReference mReferences=null;
        mReferences = mDatabase.getReference(FirebaseConstant.TAG_MIEMBRO_RED)
                .child(String.valueOf(celula.red.charAt(celula.red.length()-1)))
                .child(FirebaseConstant.TAG_CELULA).child(celula.key);
        mReferences.setValue(celula, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError==null){
                    iComplete.success(null);
                }else {
                    iComplete.failure(databaseError.toException());
                }
            }
        });
    }

    public void getCelulas(String red,final IComplete<List<Celula>> iComplete ) {
        mDatabase.getReference(FirebaseConstant.TAG_MIEMBRO_RED)
                .child(String.valueOf(red.charAt(red.length()-1)))
                .child(FirebaseConstant.TAG_CELULA)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        celulaList = new ArrayList<Celula>();
                        if(dataSnapshot.exists()){
                            Celula celula=null;
                            try{
                                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                                    celula = snapshot.getValue(Celula.class);
                                    celula.key =snapshot.getKey();
                                    celulaList.add(celula);
                                }
                            }catch (Exception e){
                                iComplete.failure(e);
                            }
                        }
                        iComplete.success(celulaList);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        iComplete.failure(databaseError.toException());
                    }
                });
    }

    public void getAsistencias(String red,final IComplete<List<AsistenciaCelula>> iComplete ) {
        mDatabase.getReference(
                FirebaseConstant.TAG_USER_RED)
            .child(String.valueOf(red.charAt(red.length()-1)))
            .child(FirebaseConstant.TAG_ASISTENCIA_CELULA)
            .addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    asistenciaCelulaList = new ArrayList<AsistenciaCelula>();
                    if(dataSnapshot.exists()){
                        AsistenciaCelula asistenciaCelula =null;
                        try{
                            for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                                asistenciaCelula = snapshot.getValue(AsistenciaCelula.class);
                                asistenciaCelula.key =snapshot.getKey();
                                asistenciaCelulaList.add(asistenciaCelula);
                            }
                        }catch (Exception e){
                            iComplete.failure(e);
                        }
                    }
                    iComplete.success(asistenciaCelulaList);
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    iComplete.failure(databaseError.toException());
                }
            });
    }


}
