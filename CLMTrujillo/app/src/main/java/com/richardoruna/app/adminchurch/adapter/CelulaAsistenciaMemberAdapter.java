package com.richardoruna.app.adminchurch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.view.ICelulaAsistenciaRedView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ricoru on 31/08/17.
 */

public class CelulaAsistenciaMemberAdapter extends RecyclerView.Adapter<CelulaAsistenciaMemberAdapter.CelulaAsistenciaMemberHolder>{

    private List<Miembro> mLista;
    private ICelulaAsistenciaRedView iCelulaAsistenciaRedView;

    public CelulaAsistenciaMemberAdapter(List<Miembro> mLista, ICelulaAsistenciaRedView iCelulaAsistenciaRedView) {
        this.mLista=mLista;
        this.iCelulaAsistenciaRedView = iCelulaAsistenciaRedView;
    }

    @Override
    public CelulaAsistenciaMemberHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View viewSender = inflater.inflate(R.layout.row_item_miembro_asistencia, viewGroup, false);
        return new CelulaAsistenciaMemberHolder(viewSender);
    }

    @Override
    public void onBindViewHolder(final CelulaAsistenciaMemberHolder holder, int position) {
        final Miembro miembro = mLista.get(position);
        holder.txt_nombre.setText(miembro.name);
        holder.chk_asistio.setChecked(miembro.asistio);
        holder.chk_asistio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                miembro.asistio = b;
                iCelulaAsistenciaRedView.onItemSeleccionado(miembro, holder.getAdapterPosition());
            }
        });
        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iCelulaAsistenciaRedView.onItemSeleccionado(miembro, holder.getAdapterPosition());
            }
        });
    }

    public List<Miembro> getList(){
        return mLista;
    }

    @Override
    public int getItemCount() {
        return mLista.size();
    }


    public class CelulaAsistenciaMemberHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.content) LinearLayout content;
        @BindView(R.id.txt_nombre) TextView txt_nombre;
        @BindView(R.id.chk_asistio) CheckBox chk_asistio;

        public CelulaAsistenciaMemberHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

    }

}
