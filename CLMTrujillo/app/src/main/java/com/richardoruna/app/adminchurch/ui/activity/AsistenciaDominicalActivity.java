package com.richardoruna.app.adminchurch.ui.activity;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.adapter.AsistenciaDominicalAdapter;
import com.richardoruna.app.adminchurch.model.entity.AsistenciaDominical;
import com.richardoruna.app.adminchurch.model.entity.Turno;
import com.richardoruna.app.adminchurch.presenter.asistencia.AsistenciaDominicalPresenter;
import com.richardoruna.app.adminchurch.presenter.asistencia.IAsistenciaDominicalPresenter;
import com.richardoruna.app.adminchurch.ui.common.SimpleDividerItemDecoration;
import com.richardoruna.app.adminchurch.view.IAsistenciaDominicalView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AsistenciaDominicalActivity extends AppCompatActivity implements IAsistenciaDominicalView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rcv_lista)
    RecyclerView rcv_lista;

    ActionBar actionBar;
    MaterialDialog progressDialog;
    AsistenciaDominicalAdapter adapter;
    IAsistenciaDominicalPresenter iAsistenciaDominicalPresenter;

    private ShareActionProvider mShareActionProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asistencia_dominical);
        ButterKnife.bind(this);

        init();
        initToolbar();
        initRecycler();
        createProgressDialog();
    }

    private void init(){
        iAsistenciaDominicalPresenter = new AsistenciaDominicalPresenter(this);
        iAsistenciaDominicalPresenter.getAsistencias();
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        //ActionBar actionBar = getSupportActionBar();
        actionBar = getSupportActionBar();
        actionBar.setTitle("");
        //actionBar.setDisplayHomeAsUpEnabled(true);
        //toolbar.setNavigationIcon(R.mipmap.ic_arrow_back);
    }

    private void initRecycler(){
        adapter = new AsistenciaDominicalAdapter(new ArrayList<AsistenciaDominical>(), this, this);
        rcv_lista.setAdapter(adapter);
        rcv_lista.setLayoutManager(new LinearLayoutManager(this));
        rcv_lista.setHasFixedSize(true);
        rcv_lista.addItemDecoration(new SimpleDividerItemDecoration(this));
        //rcv_lista.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.menu_asistencia_dominical, menu);
        menu.findItem(R.id.action_done).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_change_profile) {
            //
            return true;
        }
        if (id == R.id.action_salir) {
            iAsistenciaDominicalPresenter.clearPreferences();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void setModel(List<AsistenciaDominical> asistenciaDominicalList) {
        adapter.updateAsistenciaDominicalListItems(asistenciaDominicalList);
    }

    @Override
    public void setOptionSpinner(List<Turno> turnoList) {
    }

    @Override
    public void message(@StringRes int msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void message(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void createProgressDialog(){
        progressDialog = new MaterialDialog.Builder(this)
                .title(R.string.celula_member_title_dialog)
                .content(R.string.all_msj_dialog)
                .progress(true, 5000)
                .widgetColorRes(R.color.colorD929)
                .canceledOnTouchOutside(false)
                .cancelable(false)
                .build();
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.fab_add)
    public void add(){
        iAsistenciaDominicalPresenter.irRegistroAsistencia(this);
    }

}
