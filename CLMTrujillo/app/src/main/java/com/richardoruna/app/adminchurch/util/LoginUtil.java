package com.richardoruna.app.adminchurch.util;

import android.util.Patterns;

import java.util.regex.Pattern;

/**
 * Created by Ricoru on 11/07/17.
 */

public abstract class LoginUtil {

    public static boolean isEmailValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
        //return email.contains("@");
    }

    public static boolean isPasswordValid(String password) {
        return password.length() >= 6;
    }

}
