package com.richardoruna.app.adminchurch.presenter;

import android.support.annotation.StringRes;

import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;

/**
 * Created by Ricoru on 11/07/17.
 */

public interface IRegisterPresenter {

    void validate(String uid, String name, String email, String red, FirebaseConstant.TypeAcount typeAcount,
                  String estado);
    void error(@StringRes int msj);
    void success(Usuario usuario);

    void clearLoginFacebook();

}
