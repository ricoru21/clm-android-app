package com.richardoruna.app.adminchurch.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Ricoru on 15/07/17.
 */

public class DateUtil {

    private static SimpleDateFormat smf;
    private static String formatDefautl="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static String formatDefautlSingle = "yyyy-MM-dd";

    public static long getDateHour(){
        return new Date().getTime();
    }
    public static Date getDateNow() { return new Date();}

    public static String LongToFechaHora(String fechaLong, String format){
        String newDate="";
        try{
            long dateLong = Long.parseLong(fechaLong);
            Date date = new Date(dateLong);
            smf = new SimpleDateFormat(format, new Locale("es", "pe"));
            newDate = smf.format(date);
        }catch (Exception e){
            e.printStackTrace();
        }
        return newDate;
    }

    public static long formatStringToDate(String fecha){
        try {
            smf = new SimpleDateFormat(formatDefautlSingle);
            return smf.parse(fecha).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static Date formatISOToDate(String fecha){
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat(formatDefautl, new Locale("es", "pe"));
        df.setTimeZone(tz);
        try {
            return df.parse(fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getNowFormatISO(){
        TimeZone tz = TimeZone.getTimeZone("UTC");
        // Quoted "Z" to indicate UTC, no timezone offset
        DateFormat df = new SimpleDateFormat(formatDefautl, new Locale("es", "pe"));
        df.setTimeZone(tz);
        String nowAsISO = df.format(new Date());
        return nowAsISO;
    }

}
