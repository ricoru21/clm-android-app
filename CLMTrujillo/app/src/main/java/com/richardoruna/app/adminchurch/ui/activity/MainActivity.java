package com.richardoruna.app.adminchurch.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.adapter.ChangeProfileAdapter;
import com.richardoruna.app.adminchurch.presenter.IMainPresenter;
import com.richardoruna.app.adminchurch.presenter.MainPresenter;
import com.richardoruna.app.adminchurch.ui.fragment.CelulaFragment;
import com.richardoruna.app.adminchurch.ui.fragment.IndeliFragment;
import com.richardoruna.app.adminchurch.ui.fragment.PerfilFragment;
import com.richardoruna.app.adminchurch.view.IMainView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements IMainView {

    private static int POSITION_SELECTED=-1;

    private static int TAG_FRAGMENT_INDELI=0;
    private static int TAG_FRAGMENT_CELULA=1;
    private static int TAG_FRAGMENT_MY_ACCOUNT=2;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    Fragment fragment;
    ActionBar actionBar;

    IMainPresenter iMainPresenter;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_celula:
                    showFragment(TAG_FRAGMENT_CELULA);
                    return true;
                case R.id.navigation_indeli:
                    showFragment(TAG_FRAGMENT_INDELI);
                    return true;
                case R.id.navigation_perfil:
                    showFragment(TAG_FRAGMENT_MY_ACCOUNT);
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            //showFragment(TAG_FRAGMENT_RECYCLER);
            fragment = CelulaFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content, fragment, "").commit();
        }

        init();
        initToolbar();
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void init(){
        iMainPresenter = new MainPresenter(this);
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        //ActionBar actionBar = getSupportActionBar();
        actionBar = getSupportActionBar();
        actionBar.setTitle("");
        //actionBar.setDisplayHomeAsUpEnabled(true);
        //toolbar.setNavigationIcon(R.mipmap.ic_arrow_back);
    }

    private void showFragment(int pos){
        POSITION_SELECTED = pos;
        switch (POSITION_SELECTED){
            case 0:
                fragment = CelulaFragment.newInstance();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content, fragment, "").commit();
                break;
            case 1:
                fragment = IndeliFragment.newInstance();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content, fragment).commit();
                break;
            case 2:
                fragment = PerfilFragment.newInstance();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content, fragment).commit();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_change_profile) {
            iMainPresenter.showDialogProfile();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showDialogProfile(final ArrayList<String> mapProfile){
        LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflator.inflate(R.layout.content_dialog_profile, null);
        ListView list_change_profile = (ListView) view.findViewById(R.id.list_cuentas);
        final ChangeProfileAdapter otherCuentasAdapter =
                new ChangeProfileAdapter(MainActivity.this, mapProfile);
        list_change_profile.setAdapter(otherCuentasAdapter);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        list_change_profile.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                iMainPresenter.changePrerfenceRol(mapProfile.get(i));
                dialog.dismiss();
            }
        });
    }

    @Override
    public void irPreLogin() {
        Intent intent = new Intent(this, PreLoginActivity.class);
        startActivity(intent);
    }

}
