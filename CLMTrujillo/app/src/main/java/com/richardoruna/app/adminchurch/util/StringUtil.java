package com.richardoruna.app.adminchurch.util;

/**
 * Created by Ricoru on 17/07/17.
 */

public class StringUtil {

    public static String strUpperFirstLetter(String texto){
        StringBuilder sb = new StringBuilder(texto);
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString();
    }

    public String number2Digits(int number){
        if(number<10){
            return ("0"+number);
        }else{
            return String.valueOf(number);
        }
    }

}
