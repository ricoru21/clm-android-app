package com.richardoruna.app.adminchurch.model.dao;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;

import java.util.Map;

/**
 * Created by Ricoru on 12/07/17.
 */

public class UserDAO {

    private static FirebaseDatabase mDatabase;
    private ValueEventListener valueEventListener;

    public UserDAO(){
        mDatabase = FirebaseDatabase.getInstance();
    }

    public void save(String uid, Usuario entidad, final IComplete<Void> iComplete){
        DatabaseReference mReferences =
                mDatabase.getReference(FirebaseConstant.TAG_USER).child(uid);
        mReferences.setValue(entidad.toMap(), new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError==null){
                    iComplete.success(null);
                }else {
                    iComplete.failure(databaseError.toException());
                }
            }
        });
    }

    public void saveTokenCustomer(String uid, Map<String, String> pushAndroid, final IComplete<Void> iComplete){
        DatabaseReference mReferences = mDatabase.getReference(FirebaseConstant.TAG_USER)
                .child(uid).child("pushAndroid");
        mReferences.setValue(pushAndroid, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError==null){
                    iComplete.success(null);
                }else {
                    iComplete.failure(databaseError.toException());
                }
            }
        });
    }

    public void getProfileCustomer(String uid, final IComplete<Miembro> iComplete) {
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    try {
                        iComplete.success(dataSnapshot.getValue(Miembro.class));
                    } catch (Exception e) {
                        iComplete.failure(e);
                    }
                } else {
                    iComplete.failure(null);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                iComplete.failure(databaseError.toException());
            }
        };
        mDatabase.getReference(FirebaseConstant.TAG_USER).child(uid)
        .addValueEventListener(valueEventListener);
    }

    public void getProfileLiderSingle(String uid, final IComplete<Usuario> iComplete) {
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    try {
                        iComplete.success(dataSnapshot.getValue(Usuario.class));
                    } catch (Exception e) {
                        iComplete.failure(e);
                    }
                } else {
                    iComplete.failure(null);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                iComplete.failure(databaseError.toException());
            }
        };
        mDatabase.getReference(FirebaseConstant.TAG_USER).child(uid)
                .addListenerForSingleValueEvent(valueEventListener);
    }

}
