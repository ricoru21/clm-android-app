package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.model.dao.CelulaDAO;
import com.richardoruna.app.adminchurch.model.dao.MemberDAO;
import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.presenter.ICelulaMemberPresenter;

import java.util.List;

/**
 * Created by Ricoru on 3/09/17.
 */

public class CelulaMemberModel implements ICelulaMemberModel {

    MemberDAO memberDAO;
    CelulaDAO celulaDAO;
    ICelulaMemberPresenter iCelulaMemberPresenter;

    public CelulaMemberModel(ICelulaMemberPresenter iCelulaMemberPresenter){
        this.iCelulaMemberPresenter = iCelulaMemberPresenter;
        this.memberDAO = new MemberDAO();
        this.celulaDAO = new CelulaDAO();
    }

    @Override
    public void getCelulaMember(String red) {
        memberDAO.getMembers(red, new IComplete<List<Miembro>>() {
            @Override
            public void success(List<Miembro> objeto) {
                iCelulaMemberPresenter.data(objeto);
            }
            @Override
            public void failure(Throwable e) {
                iCelulaMemberPresenter.error(e);
            }
        });
    }

    @Override
    public void update(Celula celula) {
        celulaDAO.update(celula, new IComplete<Void>() {
            @Override
            public void success(Void objeto) {
                iCelulaMemberPresenter.success(Constant.typeAction.UPDATE);
            }

            @Override
            public void failure(Throwable e) {
                iCelulaMemberPresenter.error(e);
            }
        });
    }

}
