package com.richardoruna.app.adminchurch.presenter;

import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.CelulaRedModel;
import com.richardoruna.app.adminchurch.model.ICelulaRedModel;
import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.ui.MvpApp;
import com.richardoruna.app.adminchurch.util.Preferences;
import com.richardoruna.app.adminchurch.view.IRedCelulaView;

import java.util.List;

/**
 * Created by Ricoru on 21/09/17.
 */

public class CelulaRedPresenter implements ICelulaRedPresenter {

    private IRedCelulaView iRedCelulaView;
    private ICelulaRedModel iCelulaRedModel;
    private SharedPreferences utilPreferences;

    private Usuario usuario;
    private List<Celula> celulaList;

    public CelulaRedPresenter(IRedCelulaView iRedCelulaView) {
        this.iRedCelulaView = iRedCelulaView;
        this.iCelulaRedModel = new CelulaRedModel(this);
        utilPreferences = Preferences.getDefaultPreferences(MvpApp.getContext());
        usuario = new Gson().fromJson(
                Preferences.findStringPreference(utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY,""),
                Usuario.class);
    }

    @Override
    public void getCelulas() {
        iCelulaRedModel.getCelula(usuario.red);
    }

    @Override
    public void saveCelula(String nombre, String horario, String lugar) {
        if(TextUtils.isEmpty(nombre)){
            iRedCelulaView.message("El campo Nombre es requerido");
            return;
        }
        if(TextUtils.isEmpty(horario)){
            iRedCelulaView.message("El campo Horario es requerido");
           return;
        }
        if(TextUtils.isEmpty(lugar)){
            iRedCelulaView.message("El campo Lugar es requerido");
            return;
        }
        Celula celula = new Celula();
        celula.horario = horario;
        celula.nombre = nombre;
        celula.lugar = lugar;
        celula.red = usuario.red;

        iCelulaRedModel.saveCelula(celula);
    }

    @Override
    public void data(List<Celula> celulaList) {
        this.celulaList = celulaList;
        iRedCelulaView.setModel(celulaList);
    }

    @Override
    public void error(Throwable t) {
        if(t!=null) iRedCelulaView.message(t.getMessage());
        else iRedCelulaView.message(R.string.error_internal);
    }

    @Override
    public void success(Constant.typeAction typeAction) {
        iRedCelulaView.setModel(celulaList);
        if(typeAction.equals(Constant.typeAction.SAVE)){
            iRedCelulaView.message(R.string.success_reg_celula);
        }
    }


}
