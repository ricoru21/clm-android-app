package com.richardoruna.app.adminchurch.presenter;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.CelulaMemberModel;
import com.richardoruna.app.adminchurch.model.ICelulaMemberModel;
import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.ui.MvpApp;
import com.richardoruna.app.adminchurch.util.Preferences;
import com.richardoruna.app.adminchurch.view.ICelulaMemberView;

import java.util.Collections;
import java.util.List;

/**
 * Created by Ricoru on 21/09/17.
 */

public class CelulaMemberPresenter implements ICelulaMemberPresenter {

    ICelulaMemberView iCelulaMemberView;
    ICelulaMemberModel iCelulaMemberModel;

    private Usuario usuario;
    private List<Miembro> miembroList;
    private SharedPreferences utilPreferences;
    public CelulaMemberPresenter(ICelulaMemberView iCelulaMemberView) {
        this.iCelulaMemberView = iCelulaMemberView;
        this.iCelulaMemberModel = new CelulaMemberModel(this);
        utilPreferences = Preferences.getDefaultPreferences(MvpApp.getContext());
        this.usuario = new Gson().fromJson(
                Preferences.findStringPreference(utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY,""),
                Usuario.class);
    }

    @Override
    public void getCelulaMembers() {
        iCelulaMemberModel.getCelulaMember(usuario.red);
    }

    @Override
    public void updateMiembroCelula(Celula celula) {
        iCelulaMemberModel.update(celula);
        iCelulaMemberView.showProgress();
    }

    @Override
    public void success(Constant.typeAction typeAction) {
        if(typeAction.equals(Constant.typeAction.UPDATE)){
            iCelulaMemberView.message(R.string.success_update_celula_member);
        }
        iCelulaMemberView.hideProgress();
        iCelulaMemberView.finish();
    }

    @Override
    public void error(Throwable t) {
        iCelulaMemberView.hideProgress();
        if(t!=null) iCelulaMemberView.message(t.getMessage());
        else iCelulaMemberView.message(R.string.error_internal);
    }

    @Override
    public void data(List<Miembro> miembroList) {
        this.miembroList = miembroList;
        Collections.reverse(miembroList);
        iCelulaMemberView.setOptionCelulaMember(miembroList);
    }


}
