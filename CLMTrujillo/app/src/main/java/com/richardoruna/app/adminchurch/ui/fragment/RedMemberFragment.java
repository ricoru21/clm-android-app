package com.richardoruna.app.adminchurch.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.adapter.RedMemberAdapter;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.presenter.IMemberRedPresenter;
import com.richardoruna.app.adminchurch.presenter.MemberRedPresenter;
import com.richardoruna.app.adminchurch.view.IRedMemberView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class RedMemberFragment extends Fragment implements IRedMemberView {

    private static String ARGUMENT_DATA_ADAPTER_FIRST = "data_adapter_first";

    @BindView(R.id.rcv_lista)
    RecyclerView rcv_lista;
    @BindView(R.id.content_frame)
    FrameLayout content_frame;

    private Unbinder unbinder;
    private MaterialDialog dialogMember, dialogConfirmarDeleteMember;
    private View custom_dialog_member;
    private RedMemberAdapter adapter;
    private IMemberRedPresenter iMemberRedPresenter;

    AppCompatEditText edit_nombre;
    AppCompatEditText edit_telefono_celular;
    AppCompatEditText edit_email;

    private int position_miembro_select=-1;
    private boolean flag_editar;
    
    public RedMemberFragment() {
        // Required empty public constructor
    }

    public static RedMemberFragment newInstance() {
        RedMemberFragment fragment = new RedMemberFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public void onStart() {
        super.onStart();
        iMemberRedPresenter.getMembers();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_red_member, container, false);
        unbinder = ButterKnife.bind(this,view);
        init();
        initRecycler();
        return view;
    }


    private void init(){
        iMemberRedPresenter = new MemberRedPresenter(this);
        createDialog();
        createDialogConfirmarDelete();
    }

    private void initRecycler(){
        adapter = new RedMemberAdapter(new ArrayList<Miembro>(), this, getActivity(), true, true);//No delete
        rcv_lista.setAdapter(adapter);
        rcv_lista.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcv_lista.setHasFixedSize(true);
        rcv_lista.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        unbinder.unbind();
    }

    @Override
    public void setModel(List<Miembro> model) {
        try {
            adapter.updateMiembroListItems(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemEliminar(Miembro miembro, int position) {
        position_miembro_select = position;
        dialogConfirmarDeleteMember.show();
    }

    @Override
    public void onItemEditar(Miembro miembro, int position) {
        position_miembro_select = position;
        flag_editar = true;
        showDialog(miembro);
    }

    @Override
    public void message(@StringRes int msj) {
        Snackbar.make(content_frame, msj, Snackbar.LENGTH_SHORT)
                .setAction("Action", null)
                .show();
    }

    @Override
    public void message(String msj) {
        Snackbar.make(content_frame, msj, Snackbar.LENGTH_SHORT)
                .setAction("Action", null)
                .show();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @OnClick(R.id.fab_add)
    public void add(){
        flag_editar = false;
        showDialog(null);
    }

    private void createDialog() {
        boolean wrapInScrollView = true;
        dialogMember = new MaterialDialog.Builder(getActivity())
                .title(R.string.lbl_new_member)
                .positiveText(R.string.action_ok)
                .negativeText(R.string.action_cancel)
                .positiveColorRes(R.color.colorPrimaryDark)
                .canceledOnTouchOutside(false)
                .cancelable(false)
                .customView(R.layout.custom_dialog_member, wrapInScrollView)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        try {
                            hideDialog();
                            if(flag_editar){
                                iMemberRedPresenter.update(position_miembro_select, edit_nombre.getText().toString(), edit_telefono_celular.getText().toString(),
                                        edit_email.getText().toString());
                            }else{
                                iMemberRedPresenter.save(edit_nombre.getText().toString(), edit_telefono_celular.getText().toString(),
                                        edit_email.getText().toString());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        hideDialog();
                    }
                })
                .build();

        custom_dialog_member = dialogMember.getCustomView();
        edit_nombre = (AppCompatEditText) custom_dialog_member.findViewById(R.id.edit_nombre);
        edit_telefono_celular = (AppCompatEditText) custom_dialog_member.findViewById(R.id.edit_telefono_celular);
        edit_email = (AppCompatEditText) custom_dialog_member.findViewById(R.id.edit_email);
    }

    private void createDialogConfirmarDelete(){
        dialogConfirmarDeleteMember = new MaterialDialog.Builder(getActivity())
        .title(R.string.lbl_del_member)
        .positiveText(R.string.action_ok)
        .negativeText(R.string.action_cancel)
        .positiveColorRes(R.color.colorPrimaryDark)
        .canceledOnTouchOutside(false)
        .cancelable(false)
        .content(R.string.all_msj_delete_dialog)
        .onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                try {
                    dialogConfirmarDeleteMember.dismiss();
                    iMemberRedPresenter.remove(position_miembro_select);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        })
        .onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                dialogConfirmarDeleteMember.dismiss();
            }
        })
        .build();
    }

    private void showDialog(Miembro miembro) {
        if (miembro != null) {
            dialogMember.setTitle(R.string.lbl_udp_member);
            edit_nombre.setText(miembro.name);
            edit_telefono_celular.setText(miembro.celular);
            edit_email.setText(miembro.email);
        }else{
            edit_nombre.setText("");
            edit_telefono_celular.setText("");
            edit_email.setText("");
            dialogMember.setTitle(R.string.lbl_new_member);
        }
        dialogMember.show();
    }

    private void hideDialog() {
        dialogMember.dismiss();
    }

}
