package com.richardoruna.app.adminchurch.presenter;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.ui.MvpApp;
import com.richardoruna.app.adminchurch.util.Preferences;
import com.richardoruna.app.adminchurch.view.IPreLoginView;

/**
 * Created by Ricoru on 24/09/17.
 */

public class PreLoginPresenter implements IPreLoginPresenter {

    Usuario usuario;
    SharedPreferences utilPreferences;
    IPreLoginView iPreLoginView;

    String rol_seleccionado = "";

    public PreLoginPresenter(IPreLoginView iPreLoginView) {
        this.iPreLoginView = iPreLoginView;
        utilPreferences = Preferences.getDefaultPreferences(MvpApp.getContext());
    }

    @Override
    public void isUserLogin() {
        String uid = Preferences.findStringPreference(utilPreferences, Preferences.PREFERENCES_USER_UID, "");
        //verificar el Perfil
        if(!uid.equalsIgnoreCase("")){
            usuario = new Gson().fromJson(
                    Preferences.findStringPreference(
                            utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY, ""), Usuario.class);
            rol_seleccionado = Preferences.findStringPreference(
                    utilPreferences, Preferences.PREFERENCES_USER_ROL_SELECT, "");
            validarRoles();
        }
    }

    @Override
    public void validarRoles() {
        try{
            if( usuario.estado.equalsIgnoreCase(Constant.TAG_ESTADO_HABILITADO)) {
                if (rol_seleccionado.equalsIgnoreCase(Constant.roles.get(0))) { //invitado
                    iPreLoginView.message("No está disponible el ingreso como invitado");
                } else if (rol_seleccionado.equalsIgnoreCase(Constant.roles.get(1))) { //Es Lider o Pastor
                    iPreLoginView.irHome();
                    iPreLoginView.finish();
                } else if (rol_seleccionado.equalsIgnoreCase(Constant.roles.get(2))) { //Es Lider o Pastor
                    iPreLoginView.irHome();
                    iPreLoginView.finish();
                } else if (rol_seleccionado.equalsIgnoreCase(Constant.roles.get(3))) { //Es Ujier
                    iPreLoginView.irUjier();
                    iPreLoginView.finish();
                }
            } else if( usuario.estado.equalsIgnoreCase( Constant.TAG_ESTADO_IN_HABILITADO)){
                iPreLoginView.message("Su cuenta se encuentra inhabilitada, comuniquese con el Administrador.");
            } else if( usuario.estado.equalsIgnoreCase( Constant.TAG_ESTADO_POR_APROBAR)){
                iPreLoginView.message("Su cuenta necesita ser aprobada, intentelo más tarde");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
