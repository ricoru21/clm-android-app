package com.richardoruna.app.adminchurch.presenter;

import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.model.remote.Constant;

import java.util.List;

/**
 * Created by Ricoru on 21/09/17.
 */

public interface IMemberRedPresenter {

    void getMembers();
    void data(List<Miembro> miembroList);

    void save(String nombre, String telefono_celular, String email);
    void update(int position, String nombre, String telefono_celular, String email);
    void remove(int position);

    void success(Constant.typeAction typeAction);
    void error(Throwable e);

}
