package com.richardoruna.app.adminchurch.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.adapter.CelulaAdapter;
import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.presenter.CelulaRedPresenter;
import com.richardoruna.app.adminchurch.presenter.ICelulaRedPresenter;
import com.richardoruna.app.adminchurch.ui.activity.CelulaMemberActivity;
import com.richardoruna.app.adminchurch.view.IRedCelulaView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CelulaRedFragment extends Fragment implements IRedCelulaView {

    @BindView(R.id.rcv_lista)
    RecyclerView rcv_lista;
    @BindView(R.id.content_frame)
    FrameLayout content_frame;

    Unbinder unbinder;
    MaterialDialog dialogCelula,dialogSingleList;
    View custom_dialog_celula;
    ICelulaRedPresenter iCelulaRedPresenter;

    AppCompatEditText edit_nombre;
    AppCompatEditText edit_horario;
    AppCompatEditText edit_lugar;

    boolean flag_editar = false;
    int position_celula=-1;
    List<Celula> celulaList;

    public CelulaRedFragment() {
        // Required empty public constructor
    }

    public static CelulaRedFragment newInstance() {
        CelulaRedFragment fragment = new CelulaRedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init(){
        iCelulaRedPresenter = new CelulaRedPresenter(this);
        createDialog();
    }
    
    @Override
    public void onStart() {
        super.onStart();
        iCelulaRedPresenter.getCelulas();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_red_celula, container, false);
        unbinder = ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        unbinder.unbind();
    }

    @Override
    public void setModel(List<Celula> model) {
        try {
            celulaList = model;
            if (rcv_lista != null) {
                final CelulaAdapter adapter = new CelulaAdapter(model, this);
                rcv_lista.setAdapter(adapter);
                rcv_lista.setLayoutManager(new LinearLayoutManager(getActivity()));
                rcv_lista.setHasFixedSize(true);
                rcv_lista.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSeleccionado(final Celula celula, int position) {
        position_celula = position;
        dialogSingleList = new MaterialDialog.Builder(getActivity())
                //.title(R.string.title)
                .items(R.array.items_list_celula)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        dialogSingleList.dismiss();
                        if(getResources().getStringArray(R.array.items_list_celula)[0]
                                .toString().equalsIgnoreCase(String.valueOf(text))){
                            flag_editar = true;
                            showDialog(celula);
                        }else if(getResources().getStringArray(R.array.items_list_celula)[1]
                                .toString().equalsIgnoreCase(String.valueOf(text))){
                            eliminarCelula(celula);
                        }
                    }
                }).build();
        dialogSingleList.show();
    }

    @Override
    public void onItemMiembros(Celula celula, int position) {
        Intent intent = new Intent(getActivity(), CelulaMemberActivity.class);
        intent.putExtra(CelulaMemberActivity.ARGUMENT_ITEM_CELULA, new Gson().toJson(celula));
        startActivity(intent);
    }

    @Override
    public void message(@StringRes int msj) {
        Snackbar.make(content_frame, msj, Snackbar.LENGTH_SHORT)
                .setAction("Action", null)
                .show();
    }

    @Override
    public void message(String msj) {
        Snackbar.make(content_frame, msj, Snackbar.LENGTH_SHORT)
                .setAction("Action", null)
                .show();
    }

    @OnClick(R.id.fab_add)
    public void add(){
        flag_editar = false;
        showDialog(null);
    }

    private void createDialog() {
        boolean wrapInScrollView = true;
        dialogCelula = new MaterialDialog.Builder(getActivity())
                .title(R.string.lbl_new_celula)
                .positiveText(R.string.action_ok)
                .negativeText(R.string.action_cancel)
                .positiveColorRes(R.color.colorPrimaryDark)
                .canceledOnTouchOutside(false)
                .cancelable(false)
                .customView(R.layout.custom_dialog_celula, wrapInScrollView)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        try {
                            hideDialog();
                            iCelulaRedPresenter.saveCelula(edit_nombre.getText().toString(), edit_horario.getText().toString(),
                                    edit_lugar.getText().toString() );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        hideDialog();
                    }
                })
                .build();

        custom_dialog_celula = dialogCelula.getCustomView();
        edit_nombre = (AppCompatEditText) custom_dialog_celula.findViewById(R.id.edit_nombre);
        edit_horario = (AppCompatEditText) custom_dialog_celula.findViewById(R.id.edit_horario);
        edit_lugar = (AppCompatEditText) custom_dialog_celula.findViewById(R.id.edit_lugar);
    }

    private void showDialog(Celula celula) {
        if (celula != null) {
            dialogCelula.setTitle(R.string.lbl_udp_celula);
            edit_nombre.setText(celula.nombre);
            edit_horario.setText(celula.horario);
            edit_lugar.setText(celula.lugar);
        }else{
            limpiarCampos();
            dialogCelula.setTitle(R.string.lbl_new_celula);
        }
        dialogCelula.show();
    }

    private void hideDialog() {
        dialogCelula.dismiss();
    }

    private void eliminarCelula(Celula celula){
        celulaList.remove(celula);
        setModel(celulaList);
    }

    private void limpiarCampos(){
        edit_nombre.setText("");
        edit_horario.setText("");
        edit_lugar.setText("");
    }

}
