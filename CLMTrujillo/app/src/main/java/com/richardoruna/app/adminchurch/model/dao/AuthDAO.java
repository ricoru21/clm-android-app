package com.richardoruna.app.adminchurch.model.dao;

import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.contract.ICompleteAuth;


/**
 * Created by Ricoru on 26/07/17.
 */

public class AuthDAO {

    private static final AuthDAO ourInstance = new AuthDAO();
    private FirebaseAuth auth;

    public static AuthDAO getInstance() {
        return ourInstance;
    }

    private AuthDAO() {
        this.auth = FirebaseAuth.getInstance();
    }

    public void loginWithEmailAndPassword(String email, String password,final ICompleteAuth<AuthResult> iCompleteAuth){
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            iCompleteAuth.failure(task.getException());
                        }else{
                            iCompleteAuth.success(task.getResult());
                        }
                    }
                });
    }

    public void loginWithFacebook(String token, final ICompleteAuth<AuthResult> iCompleteAuth){
        AuthCredential credential = FacebookAuthProvider.getCredential(token);
        auth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            iCompleteAuth.success(task.getResult());
                        } else {

                            iCompleteAuth.failure(task.getException());
                        }
                    }
                });
    }

    public void loginWithGoogle(GoogleSignInAccount acct, final ICompleteAuth<AuthResult> iCompleteAuth) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        auth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            //"Authentication failed."
                            //Log.d("AuthDAO", "signInWithCredential", task.getException());
                            iCompleteAuth.failure(task.getException());
                        }else{
                            iCompleteAuth.success(task.getResult());
                        }
                    }
                });
    }

    public void createUserWithEmailAndPassword(String email,String password,final ICompleteAuth<AuthResult> iCompleteAuth){
        auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>(){
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    iCompleteAuth.failure(null);
                }else{
                    iCompleteAuth.success(task.getResult());
                }
            }
        });
    }

    public void restorePassword(String email, final IComplete<Void> iComplete){
        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            iComplete.success(null); //Email sent.
                        }else {
                            iComplete.failure(task.getException());
                        }
                    }
                });
    }

}
