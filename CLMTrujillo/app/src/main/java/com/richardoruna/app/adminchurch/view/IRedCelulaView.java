package com.richardoruna.app.adminchurch.view;

import android.support.annotation.StringRes;

import com.richardoruna.app.adminchurch.model.entity.Celula;

import java.util.List;

/**
 * Created by Ricoru on 3/09/17.
 */

public interface IRedCelulaView {

    void setModel(List<Celula> model);
    void onItemSeleccionado(Celula celula, int position);
    void onItemMiembros(Celula celula, int position);

    void message(@StringRes int msj);
    void message(String msj);

}
