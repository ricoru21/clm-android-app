package com.richardoruna.app.adminchurch.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.adapter.diffcallback.CelulaMemberDiffCallback;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.view.IRedMemberView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ricoru on 31/08/17.
 */

public class RedMemberAdapter extends RecyclerView.Adapter<RedMemberAdapter.CelulaMemberHolder>{

    private List<Miembro> mLista;
    private IRedMemberView iRedMemberView;
    private Context mContext;
    private boolean isDelete;
    private boolean isEdit;

    public RedMemberAdapter(List<Miembro> mLista, IRedMemberView iRedMemberView, Context mContext,
                            boolean isDelete, boolean isEdit) {
        this.mLista=mLista;
        this.iRedMemberView = iRedMemberView;
        this.mContext = mContext;
        this.isDelete = isDelete;
        this.isEdit = isEdit;
    }

    @Override
    public CelulaMemberHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View viewSender = inflater.inflate(R.layout.row_item_miembro_red, viewGroup, false);
        return new CelulaMemberHolder(viewSender);
    }

    @Override
    public void onBindViewHolder(final CelulaMemberHolder holder, final int position) {
        final Miembro miembro = mLista.get(position);
        if(miembro.email==null || miembro.email.equalsIgnoreCase("")){
            holder.txt_email.setVisibility(View.GONE);
        }else{
            holder.txt_email.setText(miembro.email);
            holder.txt_email.setVisibility(View.VISIBLE);
        }
        holder.txt_nombre.setText(miembro.name);
        holder.txt_telefono_celular.setText(miembro.celular);
        holder.options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(mContext, holder.options);
                //inflating menu from xml resource
                popup.inflate(R.menu.menu_option_redmember);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case 1: //R.id.action_editar
                                //handle menu1 click
                                iRedMemberView.onItemEditar(miembro, position);
                                break;
                            case 2: //R.id.action_eliminar
                                iRedMemberView.onItemEliminar(miembro, position);
                                break;
                        }
                        return false;
                    }
                });
                popup.getMenu().add(0, 1, 1, menuIconWithText(mContext.getResources().getDrawable(R.mipmap.ic_mode_edit_black_24dp), mContext.getResources().getString(R.string.action_edit)));
                popup.getMenu().add(0, 2, 2, menuIconWithText(mContext.getResources().getDrawable(R.mipmap.ic_delete_black_24dp), mContext.getResources().getString(R.string.action_delete)));
                if(isEdit){
                    popup.getMenu().findItem(1).setVisible(true);
                }else{
                    popup.getMenu().findItem(1).setVisible(false);
                }
                if(isDelete){
                    popup.getMenu().findItem(2).setVisible(true);
                }else{
                    popup.getMenu().findItem(2).setVisible(false);
                }
                //displaying the popup
                popup.show();
            }
        });
    }

    public List<Miembro> getList(){
        return mLista;
    }

    public void updateMiembroListItems(List<Miembro> miembroList) {
        final CelulaMemberDiffCallback diffCallback = new CelulaMemberDiffCallback(this.mLista, miembroList);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        this.mLista.clear();
        this.mLista.addAll(miembroList);
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public int getItemCount() {
        return mLista.size();
    }


    public class CelulaMemberHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.content) LinearLayout content;
        @BindView(R.id.txt_nombre) TextView txt_nombre;
        @BindView(R.id.txt_telefono_celular) TextView txt_telefono_celular;
        @BindView(R.id.txt_email) TextView txt_email;
        @BindView(R.id.options) TextView options;

        public CelulaMemberHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

    }

    private CharSequence menuIconWithText(Drawable r, String title) {
        r.setBounds(0, 0, r.getIntrinsicWidth(), r.getIntrinsicHeight());
        SpannableString sb = new SpannableString("    " + title);
        ImageSpan imageSpan = new ImageSpan(r, ImageSpan.ALIGN_BOTTOM);
        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sb;
    }


}
