package com.richardoruna.app.adminchurch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.view.IRedCelulaView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ricoru on 31/08/17.
 */

public class CelulaAdapter extends RecyclerView.Adapter<CelulaAdapter.CelulaHolder>{

    private List<Celula> mLista;
    private IRedCelulaView iRedCelulaView;
    private View view;

    public CelulaAdapter(List<Celula> mLista, IRedCelulaView iRedCelulaView) {
        this.mLista=mLista;
        this.iRedCelulaView = iRedCelulaView;
    }

    @Override
    public CelulaHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View viewSender = inflater.inflate(R.layout.row_item_celula, viewGroup, false);
        return new CelulaHolder(viewSender);
    }

    @Override
    public void onBindViewHolder(final CelulaHolder holder, int position) {
        final Celula celula = mLista.get(position);
        holder.txt_asistencia.setText(
                String.format(view.getResources().getString(R.string.lbl_asistentes),
                        celula.miembroList.size()));
        holder.txt_nombre.setText(celula.nombre);
        holder.txt_horario.setText(celula.horario);
        holder.txt_lugar.setText(celula.lugar);
        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iRedCelulaView.onItemSeleccionado(celula, holder.getAdapterPosition());
            }
        });
        holder.txt_asistencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iRedCelulaView.onItemMiembros(celula, holder.getAdapterPosition());
            }
        });
    }

    public List<Celula> getList(){
        return mLista;
    }

    @Override
    public int getItemCount() {
        return mLista.size();
    }


    public class CelulaHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.content) LinearLayout content;
        @BindView(R.id.txt_nombre) TextView txt_nombre;
        @BindView(R.id.txt_asistencia) TextView txt_asistencia;
        @BindView(R.id.txt_horario) TextView txt_horario;
        @BindView(R.id.txt_lugar) TextView txt_lugar;

        public CelulaHolder(View itemView) {
            super(itemView);
            view = itemView;
            ButterKnife.bind(this,itemView);
        }

    }

}
