package com.richardoruna.app.adminchurch.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.entity.AsistenciaDominical;
import com.richardoruna.app.adminchurch.model.entity.Turno;
import com.richardoruna.app.adminchurch.presenter.asistencia.AsistenciaDominicalPresenter;
import com.richardoruna.app.adminchurch.presenter.asistencia.IAsistenciaDominicalPresenter;
import com.richardoruna.app.adminchurch.util.StringUtil;
import com.richardoruna.app.adminchurch.view.IAsistenciaDominicalView;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistroAsistenciaDominicalActivity extends AppCompatActivity implements IAsistenciaDominicalView,
        CalendarDatePickerDialogFragment.OnDateSetListener{

    public static String ARGUMENT_EXTRA_SAVE = "extra_save";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.sp_turno)
    Spinner sp_turno;
    @BindView(R.id.edit_fecha)
    AppCompatEditText edit_fecha;
    @BindView(R.id.edit_nh_adultos)
    AppCompatEditText edit_nh_adultos;
    @BindView(R.id.edit_nh_jovenes)
    AppCompatEditText edit_nh_jovenes;
    @BindView(R.id.edit_nh_ninios)
    AppCompatEditText edit_nh_ninios;
    @BindView(R.id.edit_nm_adultas)
    AppCompatEditText edit_nm_adultas;
    @BindView(R.id.edit_nm_jovenes)
    AppCompatEditText edit_nm_jovenes;
    @BindView(R.id.edit_n_nuevos)
    AppCompatEditText edit_n_nuevos;

    Calendar calendar;
    ActionBar actionBar;
    StringUtil stringUtil;
    MaterialDialog progressDialog, progressDialogConfirm;
    IAsistenciaDominicalPresenter iAsistenciaDominicalPresenter;

    int year=0 , month=0, day=0;

    public static void activityFrom(Context context, Bundle extras){
        Intent intent = new Intent(context, RegistroAsistenciaDominicalActivity.class);
        intent.putExtras(extras);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_asistencia_dominical);
        ButterKnife.bind(this);

        init();
        initToolbar();
        createDialogConfirm();
        createProgressDialog();
    }

    private void init(){
        iAsistenciaDominicalPresenter = new AsistenciaDominicalPresenter(this);
        iAsistenciaDominicalPresenter.getTurnos();
        stringUtil = new StringUtil();
        calendar = Calendar.getInstance();

        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH)+1;
        year = calendar.get(Calendar.YEAR);

        edit_fecha.setText(day+ "/"+stringUtil.number2Digits(month)+"/"+year);
        if(getIntent().getExtras()!=null){
            if(getIntent().getExtras().getBoolean(ARGUMENT_EXTRA_SAVE)){

            }else{
                /*edit_nh_adultos.setText("0");
                edit_nh_jovenes.setText("0");
                edit_nh_ninios.setText("0");
                edit_nm_adultas.setText("0");
                edit_nm_jovenes.setText("0");
                edit_nm_ninias.setText("0");*/
            }
        }
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        //ActionBar actionBar = getSupportActionBar();
        actionBar = getSupportActionBar();
        actionBar.setTitle("");
        //actionBar.setDisplayHomeAsUpEnabled(true);
        //toolbar.setNavigationIcon(R.mipmap.ic_arrow_back);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_asistencia_dominical, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_done) {
            progressDialogConfirm.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setModel(List<AsistenciaDominical> asistenciaDominicalList) {

    }

    @Override
    public void setOptionSpinner(List<Turno> turnoList) {
        if (turnoList != null) {
            ArrayAdapter<List<Turno>> dataAdapter_tipo = new ArrayAdapter(this,
                    android.R.layout.simple_spinner_item, turnoList);
            /*{
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);
                    //((TextView) v).setTypeface(font);//Typeface for normal view
                    ((TextView) v).setTextSize(14f);
                    //((TextView) v).setTextColor(getResources().getColor(R.color.colorWhite));
                    AbsListView.LayoutParams lp = //new LinearLayout.LayoutParams(v.getLayoutParams());
                            new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.MATCH_PARENT);
                    lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                    lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                    v.setLayoutParams(lp);
                    v.setPadding(15,10,10,10);
                    return v;
                }
                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    //((TextView) v).setTypeface(font);//Typeface for normal view
                    ((TextView) v).setTextSize(14f);
                    AbsListView.LayoutParams lp = //new LinearLayout.LayoutParams(v.getLayoutParams());
                            new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.MATCH_PARENT);
                    lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                    lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                    v.setLayoutParams(lp);
                    v.setPadding(15,10,10,10);
                    return v;
                }
            };*/
            dataAdapter_tipo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_turno.setAdapter(dataAdapter_tipo);
        }
    }

    @Override
    public void message(@StringRes int msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void message(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.edit_fecha)
    public void showDatetimePicker(){
        //TODO validar que la fecha a seleccionar no sea mayor del día actual.
        CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(this)
                .setThemeLight()
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setPreselectedDate(year, month-1, day)
                .setDoneText("Aceptar")
                .setCancelText("Cancelar");
        cdp.show(getSupportFragmentManager(), null);
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String dateFormat= stringUtil.number2Digits(dayOfMonth)+"/";
        dateFormat+= stringUtil.number2Digits(monthOfYear+1)+"/";
        dateFormat+= year;

        this.year= year;
        this.month= monthOfYear+1;
        this.day= dayOfMonth;

        edit_fecha.setText(dateFormat);
    }

    private void createProgressDialog(){
        progressDialog = new MaterialDialog.Builder(this)
                .title(R.string.celula_member_title_dialog)
                .content(R.string.all_msj_dialog)
                .progress(true, 5000)
                .widgetColorRes(R.color.colorD929)
                .canceledOnTouchOutside(false)
                .cancelable(false)
                .build();
    }

    private void createDialogConfirm(){
        progressDialogConfirm = new MaterialDialog.Builder(this)
                .content(R.string.asistencia_dominical_dialog_confirmar)
                .positiveText(R.string.action_ok)
                .negativeText(R.string.action_cancel)
                .positiveColorRes(R.color.colorPrimaryDark)
                .neutralColorRes(R.color.colorRedLogo)
                .canceledOnTouchOutside(false)
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        progressDialogConfirm.dismiss();
                        Turno turno = (Turno) sp_turno.getSelectedItem();
                        String fecha_asistencia = year+"-"+stringUtil.number2Digits(month)+"-"+
                                stringUtil.number2Digits(day);
                        iAsistenciaDominicalPresenter.save(turno,
                                fecha_asistencia,
                                edit_fecha.getText().toString(),
                                edit_nh_adultos.getText().toString(),
                                edit_nh_jovenes.getText().toString(),
                                edit_nh_ninios.getText().toString(),
                                edit_nm_adultas.getText().toString(),
                                edit_nm_jovenes.getText().toString(),
                                edit_n_nuevos.getText().toString());
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        progressDialogConfirm.dismiss();
                    }
                })
                .build();
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

}
