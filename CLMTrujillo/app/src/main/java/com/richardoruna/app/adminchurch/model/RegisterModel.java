package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.model.dao.AuthDAO;
import com.richardoruna.app.adminchurch.model.dao.UserDAO;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;
import com.richardoruna.app.adminchurch.presenter.IRegisterPresenter;


/**
 * Created by Ricoru on 11/07/17.
 */

public class RegisterModel implements IRegisterModel {
    private static final String TAG = RegisterModel.class.getSimpleName();

    IRegisterPresenter iRegisterPresenter;
    //ILoginPresenter iLoginPresenter;
    AuthDAO authModel;
    UserDAO userDAO;

    public RegisterModel(IRegisterPresenter iRegisterPresenter) {
        authModel = AuthDAO.getInstance();
        userDAO = new UserDAO();
        this.iRegisterPresenter = iRegisterPresenter;
    }

    @Override
    public void saveRedSocial(String uid, String name, String email, String red, FirebaseConstant.TypeAcount typeAcount, String estado) {
        final Usuario usuario = new Usuario();
        usuario.name = name;
        usuario.email = email;
        usuario.red = red;
        usuario.estado = estado;
        usuario.tipoCuenta = typeAcount.name();
        userDAO.save(uid, usuario, new IComplete<Void>() {
            @Override
            public void success(Void objeto) {
                iRegisterPresenter.success(usuario);
            }
            @Override
            public void failure(Throwable throwable) {
                iRegisterPresenter.error(R.string.error_user_register);
            }
        });
    }
}
