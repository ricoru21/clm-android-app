package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.model.entity.Celula;

/**
 * Created by Ricoru on 3/09/17.
 */

public interface ICelulaMemberModel {

    void getCelulaMember(String red);
    void update(Celula celula);

}
