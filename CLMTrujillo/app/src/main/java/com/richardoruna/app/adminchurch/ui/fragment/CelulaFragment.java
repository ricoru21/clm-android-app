package com.richardoruna.app.adminchurch.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.adapter.CelulaPageAdapter;
import com.richardoruna.app.adminchurch.view.ICelulaView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class CelulaFragment extends Fragment implements ICelulaView {

    @BindView(R.id.container)
    ViewPager view_pager;
    @BindView(R.id.tab_layout)
    TabLayout tab_layout;

    private Unbinder unbinder;
    private CelulaPageAdapter adapter;

    public CelulaFragment() {
        // Required empty public constructor
    }

    public static CelulaFragment newInstance() {
        CelulaFragment fragment = new CelulaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void initViewPager(){
        adapter = new CelulaPageAdapter(getResources(), getFragmentManager());
        view_pager.setAdapter(adapter);
        tab_layout.setupWithViewPager(view_pager);
        tab_layout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(view_pager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                int index = tab.getPosition();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
                //int index = tab.getPosition();
                //BaseFragment fragment = ((BaseFragment) adapter.getFragment(index));
                //while (fragment.onBackPressed());
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_celula, container, false);
        unbinder = ButterKnife.bind(this, view);
        //aquí hacer el proceso de seteo
        initViewPager();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Glide.with(this).onDestroy();
        unbinder.unbind();
    }

    @Override
    public void message(@StringRes int msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void message(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
