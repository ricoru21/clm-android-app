package com.richardoruna.app.adminchurch.view;

import android.support.annotation.StringRes;

import com.richardoruna.app.adminchurch.model.entity.Miembro;

import java.util.List;

/**
 * Created by Ricoru on 2/09/17.
 */

public interface ICelulaMemberView {

    void setOptionCelulaMember(List<Miembro> model);
    void onItemEliminar(Miembro miembro, int position);

    void message(@StringRes int msj);
    void message(String msj);

    void showProgress();
    void hideProgress();
    void finish();

}
