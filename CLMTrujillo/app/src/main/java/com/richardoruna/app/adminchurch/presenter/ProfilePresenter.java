package com.richardoruna.app.adminchurch.presenter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.IProfileModel;
import com.richardoruna.app.adminchurch.model.ProfileModel;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;
import com.richardoruna.app.adminchurch.ui.MvpApp;
import com.richardoruna.app.adminchurch.ui.activity.EditarPerfilActivity;
import com.richardoruna.app.adminchurch.ui.activity.HistorialAsistenciaActivity;
import com.richardoruna.app.adminchurch.util.Preferences;
import com.richardoruna.app.adminchurch.view.IProfileView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by Ricoru on 17/09/17.
 */

public class ProfilePresenter implements IProfilePresenter {

    IProfileView iProfileView;
    IProfileModel iProfileModel;
    SharedPreferences utilPreferences;

    FirebaseStorage mStorage;
    StorageReference mStorageRef;
    Usuario usuario;

    public ProfilePresenter(IProfileView iProfileView) {
        this.iProfileView = iProfileView;
        iProfileModel = new ProfileModel(this);

        mStorage = FirebaseStorage.getInstance();
        mStorageRef = mStorage.getReferenceFromUrl(FirebaseConstant.TAG_STORAGE);
        utilPreferences = Preferences.getDefaultPreferences(MvpApp.getContext());
        usuario = new Gson().fromJson(
                Preferences.findStringPreference(utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY,""),
                Usuario.class);
    }

    @Override
    public void getData() {
        usuario = new Gson().fromJson(
                Preferences.findStringPreference(utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY,""),
                Usuario.class);
        if(usuario !=null) iProfileView.setData(usuario);
        else return;
    }

    @Override
    public void editarPerfil(String name, String path_foto) {
        String key = Preferences.findStringPreference(utilPreferences, Preferences.PREFERENCES_USER_UID,"");
        usuario = new Gson().fromJson(
                Preferences.findStringPreference(utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY,""),
                Usuario.class);

        if(TextUtils.isEmpty(name)){
            return;
        }

        usuario.key = key;
        iProfileView.showProgress();
        if(!path_foto.equalsIgnoreCase("")){
            if(!usuario.foto.equalsIgnoreCase(path_foto)){
                StorageReference imagesRef = mStorageRef
                        .child(FirebaseConstant.TAG_USER)
                        .child(FirebaseConstant.TAG_USER_MIEMBRO)
                        .child(key+"_perfil.jpg");
                try{
                    InputStream stream = new FileInputStream(new File(path_foto));
                    UploadTask uploadTask = imagesRef.putStream(stream);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            iProfileView.hideProgress();
                            iProfileView.message(exception.getMessage());
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Uri downloadUrl = taskSnapshot.getDownloadUrl();
                            usuario.foto = downloadUrl.toString();
                            iProfileModel.updateProfile(usuario);
                        }
                    });
                }catch (FileNotFoundException f) {
                    f.printStackTrace();
                    iProfileView.hideProgress();
                    iProfileView.message(f.getMessage());
                }catch (Exception e){
                    e.printStackTrace();
                    iProfileView.hideProgress();
                    iProfileView.message(e.getMessage());
                }
            }else{
                iProfileModel.updateProfile(usuario);
            }
        }else{
            iProfileModel.updateProfile(usuario);
        }
    }

    @Override
    public void clearPreferences() {
        Preferences.storeStringPreference(
                utilPreferences, Preferences.PREFERENCES_USER_UID, "");
        Preferences.storeStringPreference(
                utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY, "");
    }

    @Override
    public void changePrerfenceRol(String rol) {

    }

    @Override
    public void irEditarPerfil(Context mContext) {
        Intent intent = new Intent(mContext, EditarPerfilActivity.class);
        mContext.startActivity(intent);
    }

    @Override
    public void irHistorialAsistencia(Context mContext) {
        Intent intent = new Intent(mContext, HistorialAsistenciaActivity.class);
        mContext.startActivity(intent);
    }

    @Override
    public void success() {
        iProfileView.hideProgress();
        iProfileView.message(R.string.success_update_profile);
        iProfileView.finish();
    }

    @Override
    public void error(Throwable e) {
        iProfileView.hideProgress();
        if(e==null){
            iProfileView.message(R.string.error_update_profile);
        }else{
            iProfileView.message(e.toString());
        }
    }

}
