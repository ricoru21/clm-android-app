package com.richardoruna.app.adminchurch.model.entity;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ricoru on 3/09/17.
 */

@IgnoreExtraProperties
public class Celula {

    @Exclude
    public String key;

    public String nombre;
    public String horario;
    public String lugar;
    public String red;
    public List<Miembro> miembroList = new ArrayList<Miembro>();

    @Exclude
    @Override
    public String toString() {
        return nombre+" - "+horario;
    }

}
