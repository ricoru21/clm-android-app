package com.richardoruna.app.adminchurch.ui.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.adapter.CelulaMemberAdapter;
import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.presenter.CelulaMemberPresenter;
import com.richardoruna.app.adminchurch.presenter.ICelulaMemberPresenter;
import com.richardoruna.app.adminchurch.view.ICelulaMemberView;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CelulaMemberActivity extends AppCompatActivity implements ICelulaMemberView {

    public static String ARGUMENT_ITEM_CELULA = "item_celula";

    @BindView(R.id.rcv_lista)
    RecyclerView rcv_lista;
    @BindView(R.id.sp_miembro)
    SearchableSpinner sp_miembro;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    ActionBar actionBar;
    MaterialDialog progressDialog, progressDialogConfirm;
    CelulaMemberAdapter adapter;

    private Celula celula;
    private ICelulaMemberPresenter iCelulaMemberPresenter;
    private List<Miembro> miembroList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_celula_member);
        ButterKnife.bind(this);

        init();
        initToolbar();
        initRecycler();
        createDialogConfirm();
        createProgressDialog();
    }

    private void createProgressDialog(){
        progressDialog = new MaterialDialog.Builder(this)
                .title(R.string.celula_member_title_dialog)
                .content(R.string.all_msj_dialog)
                .progress(true, 5000)
                .widgetColorRes(R.color.colorD929)
                .canceledOnTouchOutside(false)
                .cancelable(false)
                .build();
    }

    private void createDialogConfirm(){
        progressDialogConfirm = new MaterialDialog.Builder(this)
                .title(R.string.celula_member_title_dialog_confirmar)
                .positiveText(R.string.action_ok)
                .negativeText(R.string.action_cancel)
                .neutralText(R.string.action_close)
                .positiveColorRes(R.color.colorPrimaryDark)
                .neutralColorRes(R.color.colorRedLogo)
                .canceledOnTouchOutside(false)
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        progressDialogConfirm.dismiss();
                        iCelulaMemberPresenter.updateMiembroCelula(celula);
                    }
                })
                .onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        progressDialogConfirm.dismiss();
                        finish();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        progressDialogConfirm.dismiss();
                    }
                })
                .build();
    }

    private void init(){
        iCelulaMemberPresenter = new CelulaMemberPresenter(this);

        if(getIntent().getExtras()!=null){
            celula = new Gson().fromJson(
                    getIntent().getStringExtra(ARGUMENT_ITEM_CELULA), Celula.class);

            if(celula!=null){
                iCelulaMemberPresenter.getCelulaMembers();
            }
        }

        sp_miembro.setTitle("Seleccionar miembro");
        miembroList = new ArrayList<Miembro>();
    }

    private void initRecycler(){
        adapter = new CelulaMemberAdapter(new ArrayList<Miembro>(), this, true); //Si delete
        rcv_lista.setAdapter(adapter);
        rcv_lista.setLayoutManager(new LinearLayoutManager(this));
        rcv_lista.setHasFixedSize(true);
        rcv_lista.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        //ActionBar actionBar = getSupportActionBar();
        actionBar = getSupportActionBar();
        actionBar.setTitle("");
        //actionBar.setDisplayHomeAsUpEnabled(true);
        //toolbar.setNavigationIcon(R.mipmap.ic_arrow_back);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_celula_member, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_done) {
            progressDialogConfirm.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setOptionCelulaMember(List<Miembro> model) {
        ArrayAdapter<Miembro> dataAdapter_condominio = new ArrayAdapter<Miembro>(this,
                android.R.layout.simple_spinner_item, model);
        /*{    public View getView(int position, View convertView, ViewGroup parent)
            {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextSize(14f);
                AbsListView.LayoutParams lp = //new LinearLayout.LayoutParams(v.getLayoutParams());
                        new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.MATCH_PARENT);
                lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                v.setLayoutParams(lp);
                v.setPadding(10,10,10,10);
                return v;
            }
            public View getDropDownView(int position, View convertView, ViewGroup parent)
            {
                View v = super.getDropDownView(position, convertView, parent);
                ((TextView) v).setTextSize(14f);
                AbsListView.LayoutParams lp = //new LinearLayout.LayoutParams(v.getLayoutParams());
                        new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.MATCH_PARENT);
                lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                v.setLayoutParams(lp);
                v.setPadding(10,10,10,10);
                return v;
            }
        };*/
        sp_miembro.setAdapter(dataAdapter_condominio);
    }

    @Override
    public void onItemEliminar(Miembro miembro, int position) {
        miembroList.remove(miembro);
        celula.miembroList = miembroList;
        setDataMiembrosCelula(miembroList);
    }

    @OnClick(R.id.btn_add_item)
    public void add(){
        Miembro miembro = (Miembro) sp_miembro.getSelectedItem();
        if(!miembroList.contains(miembro)){
            miembroList.add(miembro);
            celula.miembroList = miembroList;
            setDataMiembrosCelula(miembroList);
        }else{
            Toast.makeText(CelulaMemberActivity.this, "La persona ya fue agregado a la lista", Toast.LENGTH_SHORT).show();
        }
    }

    private void setDataMiembrosCelula(List<Miembro> data){
        miembroList = data;
        adapter.updateMiembroListItems(data);
    }

    @Override
    public void message(@StringRes int msj) {
        Toast.makeText(this, msj, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void message(String msj) {
        Toast.makeText(this, msj, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botón al pulsar ir a atrás
            progressDialogConfirm.show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
