package com.richardoruna.app.adminchurch.presenter.asistencia;

import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.model.remote.Constant;

import java.util.List;

/**
 * Created by Ricoru on 21/09/17.
 */

public interface ICelulaAsistenciaRedPresenter {

    void getCelulas();
    void data(List<Celula> celulaList);

    void save(String fechaAsistencia, Celula celula);
    void error(Throwable t);
    void success(Constant.typeAction typeAction);

}
