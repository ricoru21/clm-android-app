package com.richardoruna.app.adminchurch.adapter.diffcallback;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import com.richardoruna.app.adminchurch.model.entity.Miembro;

import java.util.List;

/**
 * Created by Ricoru on 21/09/17.
 */

public class CelulaMemberDiffCallback extends DiffUtil.Callback  {

    private final List<Miembro> mOldMiembroList;
    private final List<Miembro> mNewMiembroList;

    public CelulaMemberDiffCallback(List<Miembro> oldEmployeeList, List<Miembro> newEmployeeList) {
        this.mOldMiembroList = oldEmployeeList;
        this.mNewMiembroList = newEmployeeList;
    }

    @Override
    public int getOldListSize() {
        return mOldMiembroList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewMiembroList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        //Este método decide si dos objetos representan los mismos elementos o no.
        return mOldMiembroList.get(oldItemPosition).key.equalsIgnoreCase(mNewMiembroList.get(
                newItemPosition).key) ;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        //Este método decide si dos elementos tienen los mismos datos o no
        final Miembro oldMiembro = mOldMiembroList.get(oldItemPosition);
        final Miembro newMiembro = mNewMiembroList.get(newItemPosition);
        //return oldMiembro.name.equalsIgnoreCase(newMiembro.name);
        return oldMiembro.equals(newMiembro);
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
    
}
