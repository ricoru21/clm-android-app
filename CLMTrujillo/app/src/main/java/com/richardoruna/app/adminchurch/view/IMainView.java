package com.richardoruna.app.adminchurch.view;

import java.util.ArrayList;

/**
 * Created by Ricoru on 25/09/17.
 */

public interface IMainView {
    void showDialogProfile(final ArrayList<String> mapProfile);
    void finish();
    void irPreLogin();
}
