package com.richardoruna.app.adminchurch.adapter;

import android.content.Context;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.adapter.diffcallback.AsistenciaDominicalDiffCallback;
import com.richardoruna.app.adminchurch.model.entity.AsistenciaDominical;
import com.richardoruna.app.adminchurch.util.ExtraUtil;
import com.richardoruna.app.adminchurch.view.IAsistenciaDominicalView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ricoru on 31/08/17.
 */

public class AsistenciaDominicalAdapter extends RecyclerView.Adapter<AsistenciaDominicalAdapter.AsistenciaDominicalHolder>{

    private List<AsistenciaDominical> mLista;
    private IAsistenciaDominicalView iAsistenciaDominicalView;
    private Context mContext;

    public AsistenciaDominicalAdapter(List<AsistenciaDominical> mLista, IAsistenciaDominicalView iAsistenciaDominicalView, Context mContext) {
        this.mLista=mLista;
        this.iAsistenciaDominicalView = iAsistenciaDominicalView;
        this.mContext = mContext;
    }

    @Override
    public AsistenciaDominicalHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View viewSender = inflater.inflate(R.layout.row_item_asistencia_dominical, viewGroup, false);
        return new AsistenciaDominicalHolder(viewSender);
    }

    @Override
    public void onBindViewHolder(final AsistenciaDominicalHolder holder, final int position) {
        final AsistenciaDominical asistenciaDominical = mLista.get(position);
        holder.txt_fecha.setText("Fecha: "+asistenciaDominical.fechaMostrar);
        holder.txt_servicio.setText(asistenciaDominical.nombre_turno+" - "+ asistenciaDominical.horario_turno);
        holder.txt_nh_adultos.setText(String.valueOf(asistenciaDominical.nh_adultos));
        holder.txt_nh_jovenes.setText(String.valueOf(asistenciaDominical.nh_jovenes));
        holder.txt_nh_ninios.setText(String.valueOf(asistenciaDominical.nh_ninios));
        holder.txt_nm_adultas.setText(String.valueOf(asistenciaDominical.nm_adultas));
        holder.txt_nm_jovenes.setText(String.valueOf(asistenciaDominical.nm_jovenes));
        holder.txt_n_nuevos.setText(String.valueOf(asistenciaDominical.n_nuevos));
        int total_asistencia = asistenciaDominical.nh_adultos + asistenciaDominical.nh_jovenes +
                asistenciaDominical.nh_ninios + asistenciaDominical.nm_adultas + asistenciaDominical.nm_jovenes;
        holder.txt_total_asistencia.setText(String.valueOf(total_asistencia));

        holder.options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(mContext, holder.options);
                //inflating menu from xml resource
                popup.inflate(R.menu.menu_option_redmember);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case 1: //R.id.action_editar
                                //handle menu1 click
                                //iRedMemberView.onItemEditar(miembro, position);
                                break;
                            case 2: //R.id.action_eliminar
                                //iRedMemberView.onItemEliminar(miembro, position);
                                break;
                        }
                        return false;
                    }
                });
                popup.getMenu().add(0, 1, 1, ExtraUtil.menuIconWithText(mContext.getResources().getDrawable(R.mipmap.ic_mode_edit_black_24dp), mContext.getResources().getString(R.string.action_edit)));
                popup.getMenu().add(0, 2, 2, ExtraUtil.menuIconWithText(mContext.getResources().getDrawable(R.mipmap.ic_delete_black_24dp), mContext.getResources().getString(R.string.action_delete)));
                //displaying the popup
                popup.show();
            }
        });
    }

    public List<AsistenciaDominical> getList(){
        return mLista;
    }

    public void updateAsistenciaDominicalListItems(List<AsistenciaDominical> asistenciaDominicalList) {
        final AsistenciaDominicalDiffCallback diffCallback = new AsistenciaDominicalDiffCallback(mLista, asistenciaDominicalList);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        this.mLista.clear();
        this.mLista.addAll(asistenciaDominicalList);
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public int getItemCount() {
        return mLista.size();
    }


    public class AsistenciaDominicalHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.content) LinearLayout content;
        @BindView(R.id.txt_fecha) TextView txt_fecha;
        @BindView(R.id.txt_servicio) TextView txt_servicio;
        @BindView(R.id.txt_total_asistencia) TextView txt_total_asistencia;
        @BindView(R.id.txt_nh_adultos) TextView txt_nh_adultos;
        @BindView(R.id.txt_nh_jovenes) TextView txt_nh_jovenes;
        @BindView(R.id.txt_nh_ninios) TextView txt_nh_ninios;
        @BindView(R.id.txt_nm_adultas) TextView txt_nm_adultas;
        @BindView(R.id.txt_nm_jovenes) TextView txt_nm_jovenes;
        @BindView(R.id.txt_n_nuevos) TextView txt_n_nuevos;
        @BindView(R.id.options) TextView options;

        public AsistenciaDominicalHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

    }



}
