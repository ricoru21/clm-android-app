package com.richardoruna.app.adminchurch.presenter;

import android.content.Context;

/**
 * Created by Ricoru on 17/09/17.
 */

public interface IProfilePresenter extends IPreferencePresenter {

    void getData();
    void editarPerfil(String name,String path_foto);

    void irEditarPerfil(Context mContext);
    void irHistorialAsistencia(Context mContext);

    void success();
    void error(Throwable e);

}
