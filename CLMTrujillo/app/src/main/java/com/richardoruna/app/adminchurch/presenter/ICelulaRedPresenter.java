package com.richardoruna.app.adminchurch.presenter;

import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.model.remote.Constant;

import java.util.List;

/**
 * Created by Ricoru on 21/09/17.
 */

public interface ICelulaRedPresenter {

    void getCelulas();
    void saveCelula(String nombre,String horario,String lugar);

    void data(List<Celula> celulaList);
    void error(Throwable t);
    void success(Constant.typeAction typeAction);

}
