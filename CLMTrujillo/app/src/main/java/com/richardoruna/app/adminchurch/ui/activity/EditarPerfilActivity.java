package com.richardoruna.app.adminchurch.ui.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.presenter.IProfilePresenter;
import com.richardoruna.app.adminchurch.presenter.ProfilePresenter;
import com.richardoruna.app.adminchurch.util.MediaUtil;
import com.richardoruna.app.adminchurch.util.PermisosUtil;
import com.richardoruna.app.adminchurch.view.IProfileView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class EditarPerfilActivity extends AppCompatActivity implements IProfileView {

    private static final int REQUEST_SELECT_PICTURE_GALLERY = 10;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edit_nombre)
    AppCompatEditText edit_nombre;
    @BindView(R.id.img_foto)
    CircleImageView img_foto;
    @BindView(R.id.txt_red)
    TextView txt_red;

    ActionBar actionBar;
    MediaUtil mediaUtil;
    String path_foto_selected="";
    MaterialDialog progressDialog;
    IProfilePresenter iProfilePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil);
        ButterKnife.bind(this);
        iProfilePresenter = new ProfilePresenter(this);

        init();
        initToolbar();
        createProgressDialog();
    }

    private void init(){
        mediaUtil = new MediaUtil(this);
        iProfilePresenter.getData();
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        //ActionBar actionBar = getSupportActionBar();
        actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back);
    }

    private void createProgressDialog(){
        progressDialog = new MaterialDialog.Builder(this)
                .title(R.string.login_title_dialog)
                .content(R.string.all_msj_dialog)
                .progress(true, 5000)
                .widgetColorRes(R.color.colorD929)
                .canceledOnTouchOutside(false)
                .cancelable(false)
                .build();
    }

    @Override
    public void setData(Usuario usuario) {
        txt_red.setText(usuario.red);
        edit_nombre.setText(usuario.name);
        edit_nombre.setSelection(usuario.name.length());

        if(usuario.foto!=null){
            Glide.with(this)
                    .load(usuario.foto)
                    .override(100,100)
                    .fitCenter()
                    .crossFade()
                    .into(img_foto);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_editar_perfil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home){
            finish();
        }
        if (id == R.id.action_done) {
            iProfilePresenter.editarPerfil(edit_nombre.getText().toString(), path_foto_selected);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.img_foto)
    public void cambiarFoto(){
        selectedfoto();
    }

    private void selectedfoto() {
        if (!PermisosUtil.hasGalleryPermission()) {
            PermisosUtil.askForGalleryPermission(this);
            return;
        }
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, getString(R.string.msj_selected_gallery)), REQUEST_SELECT_PICTURE_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_SELECT_PICTURE_GALLERY){
            if(resultCode == RESULT_OK){
                if(data!=null){
                    path_foto_selected = mediaUtil.getRealPathFromURI(data.getData(), MediaUtil.TypeMedia.Image);
                    try {
                        Glide.with(this).load(path_foto_selected)
                                .fitCenter()
                                .crossFade().into(img_foto);
                        img_foto.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            selectedfoto();
        } else {
            Toast.makeText(this, R.string.error_change_foto_not_permissions, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void message(@StringRes int msg) {
        Toast.makeText(this,msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void message(String msg) {
        Toast.makeText(this,msg, Toast.LENGTH_SHORT).show();
    }

}
