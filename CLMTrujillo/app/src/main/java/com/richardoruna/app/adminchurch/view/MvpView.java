package com.richardoruna.app.adminchurch.view;

import android.support.annotation.StringRes;

/**
 * Created by Ricoru on 14/02/17.
 */

public interface MvpView {

    void message(@StringRes int msg);
    void message(String msg);

}