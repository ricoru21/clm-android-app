package com.richardoruna.app.adminchurch.model.remote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ricoru on 15/07/17.
 */

public class Constant {

    public static String TAG_URL_PUSH_NOTIFICATION = "http://wandopush.wando.pe/api/gcmdevice/send_message/";
    public static String TAG_URL_SEND_MAIL = "http://wandopush.wando.pe/api/email/send/";
    public static String TAG_TOKEN_WEB = "Token 00afa5912ddf69d17735fedd7604217f2759a8ee";
    public static String TAG_CONTENT_TYPE_WEB = "application/x-www-form-urlencoded;charset=UTF-8";

    public static enum typeAction {
        SAVE, DELETE, UPDATE
    }

    public static String TAG_ESTADO_HABILITADO = "Habilitado";
    public static String TAG_ESTADO_IN_HABILITADO = "Inhabilitado";
    public static String TAG_ESTADO_POR_APROBAR = "PorAprobar";

    public static ArrayList<String> roles = new ArrayList<String>(){{
        add("Inivitado");
        add("LiderRed");
        add("LiderCelula");
        add("Ujier");
    }};

    public static Map<String, String> roles_name = new HashMap<String, String>(){{
        put("Inivitado","Invitado");
        put("LiderRed","Lider de Red");
        put("LiderCelula","Lider de Célula");
        put("Ujier","Ujier");
    }};


}

