package com.richardoruna.app.adminchurch.presenter.asistencia;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.CelulaAsistenciaRedModel;
import com.richardoruna.app.adminchurch.model.ICelulaAsistenciaRedModel;
import com.richardoruna.app.adminchurch.model.entity.AsistenciaCelula;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.ui.MvpApp;
import com.richardoruna.app.adminchurch.util.Preferences;
import com.richardoruna.app.adminchurch.view.IHistorialAsistenciaView;

import java.util.Collections;
import java.util.List;

/**
 * Created by Ricoru on 18/09/17.
 */

public class HistorialAsistenciaPresenter implements IHistorialAsistenciaPresenter {

    IHistorialAsistenciaView iHistorialAsistenciaView;
    ICelulaAsistenciaRedModel iCelulaAsistenciaRedModel;

    Usuario usuario;
    SharedPreferences utilPreferences;

    public HistorialAsistenciaPresenter(IHistorialAsistenciaView iHistorialAsistenciaView) {
        this.iHistorialAsistenciaView = iHistorialAsistenciaView;
        this.iCelulaAsistenciaRedModel = new CelulaAsistenciaRedModel(null,this);
        utilPreferences = Preferences.getDefaultPreferences(MvpApp.getContext());
        this.usuario = new Gson().fromJson(
                Preferences.findStringPreference(utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY,""),
                Usuario.class);
    }


    @Override
    public void getAsistencias() {
        iCelulaAsistenciaRedModel.getAsistencias(usuario.red);
    }

    @Override
    public void data(List<AsistenciaCelula> asistenciaCelulaList) {
        Collections.reverse(asistenciaCelulaList);
        iHistorialAsistenciaView.setModel(asistenciaCelulaList);
    }

    @Override
    public void error(Throwable t) {
        if(t!=null){
            iHistorialAsistenciaView.message(t.getMessage());
        }else{
            iHistorialAsistenciaView.message(R.string.error_not_data_historial_asistencia);
        }
    }


}
