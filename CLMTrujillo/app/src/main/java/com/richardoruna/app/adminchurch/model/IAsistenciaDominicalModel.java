package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.model.entity.AsistenciaDominical;

/**
 * Created by Ricoru on 22/09/17.
 */

public interface IAsistenciaDominicalModel {

    void getAsistencias();

    void getTurnos();
    void save(AsistenciaDominical asistenciaDominical);

}
