package com.richardoruna.app.adminchurch.presenter;

import android.content.SharedPreferences;
import android.support.annotation.StringRes;
import android.text.TextUtils;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.IRegisterModel;
import com.richardoruna.app.adminchurch.model.RegisterModel;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;
import com.richardoruna.app.adminchurch.ui.MvpApp;
import com.richardoruna.app.adminchurch.util.LoginUtil;
import com.richardoruna.app.adminchurch.util.Preferences;
import com.richardoruna.app.adminchurch.view.IRegisterView;


/**
 * Created by Ricoru on 11/07/17.
 */

public class RegisterPresenter implements IRegisterPresenter {
    private static final String TAG = RegisterPresenter.class.getSimpleName();

    private IRegisterView view;
    private IRegisterModel registerModel;
    private SharedPreferences utilPreferences;

    String temp_uid="";

    public RegisterPresenter(IRegisterView view) {
        this.view = view;
        registerModel = new RegisterModel(this);
        utilPreferences = Preferences.getDefaultPreferences(MvpApp.getContext());
    }

    @Override
    public void validate(String uid, String name, String email, String red, FirebaseConstant.TypeAcount typeAcount, String estado) {
        temp_uid = uid;
        if(TextUtils.isEmpty(name)){
            view.message(R.string.error_field_required);
            return;
        }
        if(TextUtils.isEmpty(email)){
            view.message(R.string.error_field_required);
            return;
        }
        if(TextUtils.isEmpty(red)){
            view.message(R.string.error_field_required);
            return;
        }
        if(!LoginUtil.isEmailValid(email)){
            view.message(R.string.error_invalid_email);
            return;
        }

        view.showProgress();
        registerModel.saveRedSocial(uid, name, email, red, typeAcount, estado);
    }

    @Override
    public void error(@StringRes int msj) {
        view.message(msj);
        view.hideProgress();
    }

    @Override
    public void success(Usuario usuario) {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        usuario.foto = firebaseUser.getPhotoUrl().toString();
        Preferences.storeStringPreference(utilPreferences, Preferences.PREFERENCES_USER_UID, firebaseUser.getUid());
        Preferences.storeStringPreference(
                utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY, new Gson().toJson(usuario));
        view.message(R.string.success_user_register);
        view.hideProgress();
        view.finish();
    }

    @Override
    public void clearLoginFacebook() {
        LoginManager.getInstance().logOut();
    }

}
