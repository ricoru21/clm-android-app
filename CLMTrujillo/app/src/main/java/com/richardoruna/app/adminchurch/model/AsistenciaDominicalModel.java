package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.model.dao.AsistenciaDAO;
import com.richardoruna.app.adminchurch.model.dao.TurnoDAO;
import com.richardoruna.app.adminchurch.model.entity.AsistenciaDominical;
import com.richardoruna.app.adminchurch.model.entity.Turno;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.presenter.asistencia.IAsistenciaDominicalPresenter;

import java.util.List;

/**
 * Created by Ricoru on 22/09/17.
 */

public class AsistenciaDominicalModel implements IAsistenciaDominicalModel {

    IAsistenciaDominicalPresenter iAsistenciaDominicalPresenter;
    TurnoDAO turnoDAO;
    AsistenciaDAO asistenciaDAO;
    public AsistenciaDominicalModel(IAsistenciaDominicalPresenter iAsistenciaDominicalPresenter) {
        this.iAsistenciaDominicalPresenter = iAsistenciaDominicalPresenter;
        this.turnoDAO = new TurnoDAO();
        this.asistenciaDAO = new AsistenciaDAO();
    }

    @Override
    public void getAsistencias() {
        asistenciaDAO.getAsistenciaDominical(new IComplete<List<AsistenciaDominical>>() {
            @Override
            public void success(List<AsistenciaDominical> objeto) {
                iAsistenciaDominicalPresenter.data(objeto);
            }
            @Override
            public void failure(Throwable e) {
                iAsistenciaDominicalPresenter.error(e);
            }
        });
    }

    @Override
    public void getTurnos() {
        turnoDAO.getTurnos(new IComplete<List<Turno>>() {
            @Override
            public void success(List<Turno> objeto) {
                iAsistenciaDominicalPresenter.dataTurnos(objeto);
            }
            @Override
            public void failure(Throwable e) {
                iAsistenciaDominicalPresenter.error(e);
            }
        });
    }

    @Override
    public void save(AsistenciaDominical asistenciaDominical) {
        asistenciaDAO.saveAsistenciaDominical(asistenciaDominical, new IComplete<Void>() {
            @Override
            public void success(Void objeto) {
                iAsistenciaDominicalPresenter.success(Constant.typeAction.SAVE);
            }

            @Override
            public void failure(Throwable e) {
                iAsistenciaDominicalPresenter.error(e);
            }
        });
    }

}
