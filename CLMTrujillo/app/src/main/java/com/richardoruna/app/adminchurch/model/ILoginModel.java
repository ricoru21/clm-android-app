package com.richardoruna.app.adminchurch.model;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/**
 * Created by Ricoru on 11/07/17.
 */

public interface ILoginModel {

    void loginFacebook(String token);
    void loginGoogle(GoogleSignInAccount account);
    void restorePassword(String email);

}
