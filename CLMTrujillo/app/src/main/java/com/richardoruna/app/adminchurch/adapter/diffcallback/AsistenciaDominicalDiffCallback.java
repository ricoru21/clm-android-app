package com.richardoruna.app.adminchurch.adapter.diffcallback;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import com.richardoruna.app.adminchurch.model.entity.AsistenciaDominical;

import java.util.List;

/**
 * Created by Ricoru on 21/09/17.
 */

public class AsistenciaDominicalDiffCallback extends DiffUtil.Callback  {

    private final List<AsistenciaDominical> mOldAsistenciaDominicalList;
    private final List<AsistenciaDominical> mNewAsistenciaDominicalList;

    public AsistenciaDominicalDiffCallback(List<AsistenciaDominical> oldEmployeeList, List<AsistenciaDominical> newEmployeeList) {
        this.mOldAsistenciaDominicalList = oldEmployeeList;
        this.mNewAsistenciaDominicalList = newEmployeeList;
    }

    @Override
    public int getOldListSize() {
        return mOldAsistenciaDominicalList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewAsistenciaDominicalList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        //Este método decide si dos objetos representan los mismos elementos o no.
        return mOldAsistenciaDominicalList.get(oldItemPosition).key.equalsIgnoreCase(mNewAsistenciaDominicalList.get(
                newItemPosition).key) ;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final AsistenciaDominical oldMiembro = mOldAsistenciaDominicalList.get(oldItemPosition);
        final AsistenciaDominical newMiembro = mNewAsistenciaDominicalList.get(newItemPosition);
        return oldMiembro.equals(newMiembro);
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
    
}
