package com.richardoruna.app.adminchurch.model.dao;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.model.entity.AsistenciaCelula;
import com.richardoruna.app.adminchurch.model.entity.AsistenciaDominical;
import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ricoru on 3/09/17.
 */

public class AsistenciaDAO {

    private static FirebaseDatabase mDatabase;
    private List<AsistenciaDominical> asistenciaDominicalList;

    public AsistenciaDAO(){
        mDatabase = FirebaseDatabase.getInstance();
    }

    public void save(AsistenciaCelula asistenciaCelula, final IComplete<Void> iComplete){
        DatabaseReference mReferences=null;
        mReferences = mDatabase.getReference(FirebaseConstant.TAG_MIEMBRO_RED)
                .child(String.valueOf(asistenciaCelula.celula.red.charAt(asistenciaCelula.celula.red.length()-1)))
                .child(FirebaseConstant.TAG_ASISTENCIA_CELULA);
        mReferences.push().setValue(asistenciaCelula, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError==null){
                    iComplete.success(null);
                }else {
                    iComplete.failure(databaseError.toException());
                }
            }
        });
    }

    public void saveAsistenciaDominical(AsistenciaDominical asistenciaDominical, final IComplete<Void> iComplete){
        DatabaseReference mReferences=null;
        mReferences = mDatabase.getReference(FirebaseConstant.TAG_ASISTENCIA_DOMINICAL);
        mReferences.push().setValue(asistenciaDominical, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError==null){
                    iComplete.success(null);
                }else {
                    iComplete.failure(databaseError.toException());
                }
            }
        });
    }

    public void getAsistenciaDominical(final IComplete<List<AsistenciaDominical>> iComplete ) {
        mDatabase.getReference(FirebaseConstant.TAG_ASISTENCIA_DOMINICAL)
            .addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    asistenciaDominicalList = new ArrayList<AsistenciaDominical>();
                    if(dataSnapshot.exists()){
                        try{
                            for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                                AsistenciaDominical asistenciaDominical = snapshot.getValue(AsistenciaDominical.class);
                                asistenciaDominical.key =snapshot.getKey();
                                asistenciaDominicalList.add(asistenciaDominical);
                            }
                        }catch (Exception ex){
                            iComplete.failure(ex);
                        }
                    }
                    iComplete.success(asistenciaDominicalList);
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    iComplete.failure(databaseError.toException());
                }
            });
    }

}
