package com.richardoruna.app.adminchurch.view;

import android.os.Bundle;
import android.support.annotation.StringRes;

/**
 * Created by Ricoru on 15/02/17.
 */

public interface ILoginView extends MvpView {

    void irRegisterAccount(Bundle extra);
    void irHome(); //LiderRed o LiderCelula o Pastor
    void irUjier();

    void showProgress(@StringRes int title);
    void hideProgress();
    void finish();

    void clearLogginManager();

}
