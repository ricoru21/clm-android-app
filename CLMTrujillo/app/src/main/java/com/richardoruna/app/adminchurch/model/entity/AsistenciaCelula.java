package com.richardoruna.app.adminchurch.model.entity;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ricoru on 3/09/17.
 */

@IgnoreExtraProperties
public class AsistenciaCelula {

    @Exclude
    public String key;
    public String fechaRegistro;
    public String fechaAsistencia;
    public int nroAsistido;
    public int nroMiembros;
    public Celula celula;

    public List<Miembro> miembroList = new ArrayList<Miembro>();

}
