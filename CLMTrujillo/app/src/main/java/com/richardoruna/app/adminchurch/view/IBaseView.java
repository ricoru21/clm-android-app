package com.richardoruna.app.adminchurch.view;

import android.support.annotation.StringRes;

/**
 * Created by Ricoru on 15/08/17.
 */

public interface IBaseView {

    void message(@StringRes int msg);
    void message(String msg);

}
