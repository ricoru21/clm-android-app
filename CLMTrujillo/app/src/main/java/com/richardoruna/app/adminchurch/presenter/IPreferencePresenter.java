package com.richardoruna.app.adminchurch.presenter;

/**
 * Created by Ricoru on 25/09/17.
 */

public interface IPreferencePresenter {
    void clearPreferences();

    void changePrerfenceRol(String rol);
}
