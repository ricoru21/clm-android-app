package com.richardoruna.app.adminchurch.model.entity;

import android.support.annotation.Keep;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Ricoru on 22/09/17.
 */

/*evita que esta clase se elimen los getters/setters debido a la activación en el gradle
para reducir código y recursos*/
@Keep
@IgnoreExtraProperties
public class Turno {

    @Exclude
    public String key;
    public String horario;
    public String nombre;

    public Turno() {
    }

    @Override
    public String toString() {
        return nombre+" - "+horario;
    }
}
