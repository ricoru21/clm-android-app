package com.richardoruna.app.adminchurch.lib.base;

/**
 * Created by Ricoru on 15/02/17.
 */

public interface EventBus {
    void register(Object subscriber);
    void unregister(Object subscriber);
    void post(Object event);
}
