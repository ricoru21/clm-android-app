package com.richardoruna.app.adminchurch.model.entity;

import android.support.annotation.Keep;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ricoru on 14/08/17.
 */

@Keep
@IgnoreExtraProperties
public class Usuario {

    @Exclude
    public String key;
    @PropertyName("nombre")
    public String name;
    public String email;
    public String red;
    public String tipoCuenta;
    @PropertyName("urlFoto")
    public String foto;
    public String estado; //habilitado, inhabiltado, aprobacion si necesita aprovación solo en caso de Lideres
    //TODO posiblemente tengamos un mapa de roles
    //public HashMap<String, Object> roles;//Invitado, Lider de Red, Ugier, Maestro de INDELI
    public ArrayList<String> roles;

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("nombre", name);
        result.put("email", email);
        result.put("red", red);
        result.put("tipoCuenta", tipoCuenta);
        result.put("urlFoto", foto);
        result.put("estado", estado);
        return result;
    }

}
