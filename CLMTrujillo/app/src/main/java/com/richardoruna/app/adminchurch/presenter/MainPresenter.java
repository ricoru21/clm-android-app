package com.richardoruna.app.adminchurch.presenter;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.ui.MvpApp;
import com.richardoruna.app.adminchurch.util.Preferences;
import com.richardoruna.app.adminchurch.view.IMainView;

/**
 * Created by Ricoru on 25/09/17.
 */

public class MainPresenter implements IMainPresenter {

    IMainView iMainView;

    SharedPreferences utilPreferences;
    Usuario usuario;

    public MainPresenter(IMainView iMainView) {
        this.iMainView = iMainView;
        this.utilPreferences = Preferences.getDefaultPreferences(MvpApp.getContext());
        usuario = new Gson().fromJson(
                Preferences.findStringPreference(
                        utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY, ""), Usuario.class);
    }

    @Override
    public void showDialogProfile() {
        iMainView.showDialogProfile(usuario.roles);
    }

    @Override
    public void clearPreferences() {

    }

    @Override
    public void changePrerfenceRol(String rol) {
        String rol_selected = Preferences.findStringPreference(utilPreferences, Preferences.PREFERENCES_USER_ROL_SELECT,"");
        if(!rol.equalsIgnoreCase(rol_selected)){
            Preferences.storeStringPreference(
                    utilPreferences, Preferences.PREFERENCES_USER_ROL_SELECT, rol);
            iMainView.irPreLogin();
        }
    }

}
