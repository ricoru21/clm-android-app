package com.richardoruna.app.adminchurch.model.dao;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.model.entity.Turno;
import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ricoru on 22/09/17.
 */

public class TurnoDAO {

    private static FirebaseDatabase mDatabase;
    private List<Turno> turnoList;

    public TurnoDAO(){
        mDatabase = FirebaseDatabase.getInstance();
    }

    public void getTurnos(final IComplete<List<Turno>> iComplete ) {
        mDatabase.getReference(FirebaseConstant.TAG_TURNOS)
        .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                turnoList = new ArrayList<Turno>();
                if(dataSnapshot.exists()){
                    /*try{
                        GenericTypeIndicator<List<Turno>> t = new GenericTypeIndicator <List<Turno>>() {};
                        turnoList = dataSnapshot.getValue(t);
                    }catch (Exception e){
                        iComplete.failure(e);
                    }*/
                    try{
                        for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                            Turno turno = snapshot.getValue(Turno.class);
                            turno.key =snapshot.getKey();
                            turnoList.add(turno);
                        }
                    }catch (Exception ex){
                        iComplete.failure(ex);
                    }
                }
                iComplete.success(turnoList);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                iComplete.failure(databaseError.toException());
            }
        });
    }


}
