package com.richardoruna.app.adminchurch.model.entity;

import android.support.annotation.Keep;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Ricoru on 23/09/17.
 */

@Keep
@IgnoreExtraProperties
public class AsistenciaDominical {

    @Exclude
    public String key;

    public String key_turno;
    public String nombre_turno;
    public String horario_turno;
    public String fechaMostrar;
    public long fecha;
    public int nh_adultos;
    public int nh_jovenes;
    public int nh_ninios;
    public int nm_adultas;
    public int nm_jovenes;
    public int n_nuevos;

    public AsistenciaDominical() {

    }

}
