package com.richardoruna.app.adminchurch.model.remote;

/**
 * Created by Ricoru on 11/07/17.
 */

public class FirebaseConstant {

    public enum TypeAcount {
        Facebook, Google
    }

    public static String TAG_USER = "usuarios";
    public static String TAG_USER_RED = "red";
    public static String TAG_USER_MIEMBRO = "miembros";
    public static String TAG_TURNOS = "turnos";
    public static String TAG_ASISTENCIA_DOMINICAL = "asistencia_dominical";

    public static String TAG_MIEMBRO = "miembros";
    public static String TAG_CELULA = "celulas";
    public static String TAG_ASISTENCIA_CELULA = "asistencia_celulas";
    public static String TAG_MIEMBRO_RED = "red";

    public static String TAG_STORAGE = "gs://adminchurch-ea4d9.appspot.com";

}