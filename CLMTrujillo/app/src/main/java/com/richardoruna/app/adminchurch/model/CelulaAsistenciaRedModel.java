package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.model.dao.AsistenciaDAO;
import com.richardoruna.app.adminchurch.model.dao.CelulaDAO;
import com.richardoruna.app.adminchurch.model.entity.AsistenciaCelula;
import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.presenter.asistencia.ICelulaAsistenciaRedPresenter;
import com.richardoruna.app.adminchurch.presenter.asistencia.IHistorialAsistenciaPresenter;

import java.util.List;

/**
 * Created by Ricoru on 21/09/17.
 */

public class CelulaAsistenciaRedModel implements ICelulaAsistenciaRedModel {

    AsistenciaDAO asistenciaDAO;
    CelulaDAO celulaDAO;

    ICelulaAsistenciaRedPresenter iCelulaAsistenciaRedPresenter;
    IHistorialAsistenciaPresenter iHistorialAsistenciaPresenter;
    public CelulaAsistenciaRedModel(ICelulaAsistenciaRedPresenter iCelulaAsistenciaRedPresenter,
                                    IHistorialAsistenciaPresenter iHistorialAsistenciaPresenter) {
        this.asistenciaDAO = new AsistenciaDAO();
        this.celulaDAO = new CelulaDAO();
        this.iCelulaAsistenciaRedPresenter = iCelulaAsistenciaRedPresenter;
        this.iHistorialAsistenciaPresenter = iHistorialAsistenciaPresenter;
    }

    @Override
    public void save(AsistenciaCelula asistenciaCelula){
        asistenciaDAO.save(asistenciaCelula, new IComplete<Void>() {
            @Override
            public void success(Void objeto) {
                iCelulaAsistenciaRedPresenter.success(Constant.typeAction.SAVE);
            }
            @Override
            public void failure(Throwable e) {
                iCelulaAsistenciaRedPresenter.error(e);
            }
        });
    }

    @Override
    public void getCelula(String red) {
        celulaDAO.getCelulas(red, new IComplete<List<Celula>>() {
            @Override
            public void success(List<Celula> objeto) {
                iCelulaAsistenciaRedPresenter.data(objeto);
            }
            @Override
            public void failure(Throwable e) {
                iCelulaAsistenciaRedPresenter.error(e);
            }
        });
    }

    @Override
    public void getAsistencias(String red) {
        celulaDAO.getAsistencias(red, new IComplete<List<AsistenciaCelula>>() {
            @Override
            public void success(List<AsistenciaCelula> objeto) {
                iHistorialAsistenciaPresenter.data(objeto);
            }
            @Override
            public void failure(Throwable e) {
                iHistorialAsistenciaPresenter.error(e);
            }
        });
    }

}
