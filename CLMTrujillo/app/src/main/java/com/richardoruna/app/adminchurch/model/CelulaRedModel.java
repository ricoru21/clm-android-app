package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.model.dao.CelulaDAO;
import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.presenter.ICelulaRedPresenter;

import java.util.List;

/**
 * Created by Ricoru on 21/09/17.
 */

public class CelulaRedModel<T> implements ICelulaRedModel {

    CelulaDAO celulaDAO;
    ICelulaRedPresenter iCelulaRedPresenter;
    public CelulaRedModel(ICelulaRedPresenter iCelulaRedPresenter) {
        this.iCelulaRedPresenter = iCelulaRedPresenter;
        this.celulaDAO = new CelulaDAO();
    }

    @Override
    public void getCelula(String red) {
        celulaDAO.getCelulas(red, new IComplete<List<Celula>>() {
            @Override
            public void success(List<Celula> objeto) {
                iCelulaRedPresenter.data(objeto);
            }

            @Override
            public void failure(Throwable e) {
                iCelulaRedPresenter.error(e);
            }
        });
    }

    @Override
    public void saveCelula(Celula celula) {
        celulaDAO.save(celula, new IComplete<Void>() {
            @Override
            public void success(Void objeto) {
                iCelulaRedPresenter.success(Constant.typeAction.SAVE);
            }

            @Override
            public void failure(Throwable e) {
                iCelulaRedPresenter.error(e);
            }
        });
    }


}
