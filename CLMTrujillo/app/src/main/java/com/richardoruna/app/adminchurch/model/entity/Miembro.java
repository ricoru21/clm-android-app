package com.richardoruna.app.adminchurch.model.entity;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ricoru on 14/08/17.
 */
@IgnoreExtraProperties
public class Miembro {

    @Exclude
    public String key;
    @PropertyName("nombre")
    public String name;
    public String email;
    public String red;
    public String celular;
    
    public boolean asistio=false;

    public Miembro() {

    }

    @Exclude
    @Override
    public String toString() {
        return name;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("nombre", name);
        result.put("email", email);
        result.put("red", red);
        result.put("celular", celular);
        return result;
    }
}
