package com.richardoruna.app.adminchurch.view;

import android.support.annotation.StringRes;

import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.model.entity.Miembro;

import java.util.List;

/**
 * Created by Ricoru on 3/09/17.
 */

public interface ICelulaAsistenciaRedView {

    void dataOptionSpinner(List<Celula> celulaList);
    void onItemSeleccionado(Miembro miembro, int position);

    void message(@StringRes int msj);
    void message(String msj);

    void clearData();
    void showProgress();
    void hideProgress();

}
