package com.richardoruna.app.adminchurch.adapter;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.adapter.diffcallback.CelulaMemberDiffCallback;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.view.ICelulaMemberView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ricoru on 31/08/17.
 */

public class CelulaMemberAdapter extends RecyclerView.Adapter<CelulaMemberAdapter.CelulaMemberHolder>{

    private List<Miembro> mLista;
    private ICelulaMemberView iCelulaMemberView;
    private boolean isDelete;

    public CelulaMemberAdapter(List<Miembro> mLista, ICelulaMemberView iCelulaMemberView,
                               boolean isDelete) {
        this.mLista=mLista;
        this.iCelulaMemberView = iCelulaMemberView;
        this.isDelete = isDelete;
    }

    @Override
    public CelulaMemberHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View viewSender = inflater.inflate(R.layout.row_item_miembro_celula, viewGroup, false);
        return new CelulaMemberHolder(viewSender);
    }

    @Override
    public void onBindViewHolder(final CelulaMemberHolder holder, final int position) {
        final Miembro miembro = mLista.get(position);
        if(miembro.email==null || miembro.email.equalsIgnoreCase("")){
            holder.txt_email.setVisibility(View.GONE);
        }else{
            holder.txt_email.setText(miembro.email);
            holder.txt_email.setVisibility(View.VISIBLE);
        }
        holder.txt_nombre.setText(miembro.name);
        holder.txt_telefono_celular.setText(miembro.celular);
        holder.btn_eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iCelulaMemberView.onItemEliminar(miembro, position);
            }
        });
        if(isDelete){
            holder.btn_eliminar.setVisibility(View.VISIBLE);
        }else{
            holder.btn_eliminar.setVisibility(View.GONE);
        }
    }

    public List<Miembro> getList(){
        return mLista;
    }

    public void updateMiembroListItems(List<Miembro> miembroList) {
        final CelulaMemberDiffCallback diffCallback = new CelulaMemberDiffCallback(this.mLista, miembroList);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        this.mLista.clear();
        this.mLista.addAll(miembroList);
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public int getItemCount() {
        return mLista.size();
    }


    public class CelulaMemberHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.content) LinearLayout content;
        @BindView(R.id.txt_nombre) TextView txt_nombre;
        @BindView(R.id.txt_telefono_celular) TextView txt_telefono_celular;
        @BindView(R.id.txt_email) TextView txt_email;
        @BindView(R.id.btn_eliminar) AppCompatImageView btn_eliminar;

        public CelulaMemberHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

    }

}
