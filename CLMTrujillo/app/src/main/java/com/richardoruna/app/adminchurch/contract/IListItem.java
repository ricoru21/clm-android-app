package com.richardoruna.app.adminchurch.contract;

/**
 * Created by Ricoru on 15/07/17.
 */

public interface IListItem<T> {
    void add(T item);
    void update(T item);
    void delete(T item);
}
