package com.richardoruna.app.adminchurch.adapter;

import android.content.res.Resources;
import android.support.v4.app.FixedFragmentStatePagerAdapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.SparseArray;
import android.view.ViewGroup;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.ui.fragment.CelulaAsistanceRedFragment;
import com.richardoruna.app.adminchurch.ui.fragment.RedMemberFragment;
import com.richardoruna.app.adminchurch.ui.fragment.CelulaRedFragment;

/**
 * Created by Ricoru on 15/08/17.
 */

public class CelulaPageAdapter extends FixedFragmentStatePagerAdapter {

    private final Resources resources;
    private SparseArray<Fragment> fragments = new SparseArray<Fragment>();

    public CelulaPageAdapter(final Resources resources, FragmentManager fm) {
        super(fm);
        this.resources = resources;
    }

    @Override
    public Fragment getItem(int position) {
        final Fragment result;
        switch (position) {
            case 0:
                result = RedMemberFragment.newInstance();
                break;
            case 1:
                result = CelulaRedFragment.newInstance();
                break;
            case 2:
                result = CelulaAsistanceRedFragment.newInstance();
                break;
            default:
                result = null;
                break;
        }
        return result;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(final int position) {
        switch (position) {
            case 0:
                return resources.getString(R.string.title_tab_celula_member);
            case 1:
                return resources.getString(R.string.title_tab_celula_celulas);
            case 2:
                return resources.getString(R.string.title_tab_celula_asistance);
            default:
                return null;
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        fragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        fragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getFragment(int position) {
        return fragments.get(position);
    }

}
