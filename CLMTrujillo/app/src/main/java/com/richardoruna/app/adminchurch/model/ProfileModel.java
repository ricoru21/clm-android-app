package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.model.dao.UserDAO;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.presenter.IProfilePresenter;

/**
 * Created by Ricoru on 18/09/17.
 */

public class ProfileModel implements IProfileModel {

    UserDAO userDAO;
    IProfilePresenter iProfilePresenter;

    public ProfileModel(IProfilePresenter iProfilePresenter) {
        this.iProfilePresenter = iProfilePresenter;
        this.userDAO = new UserDAO();
    }

    @Override
    public void updateProfile(Usuario usuario) {
        userDAO.save(usuario.key, usuario, new IComplete<Void>() {
            @Override
            public void success(Void objeto) {
                iProfilePresenter.success();
            }
            @Override
            public void failure(Throwable throwable) {
                iProfilePresenter.error(throwable);
            }
        });
    }

}
