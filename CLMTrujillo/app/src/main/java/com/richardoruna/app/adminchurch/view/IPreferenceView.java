package com.richardoruna.app.adminchurch.view;

import java.util.ArrayList;

/**
 * Created by Ricoru on 25/09/17.
 */

public interface IPreferenceView {

    void showDialogProfile(ArrayList<String> mapProfile);

}
