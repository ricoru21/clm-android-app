package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.model.entity.Miembro;

/**
 * Created by Ricoru on 21/09/17.
 */

public interface IMemberRedModel {

    void getMember(String red);
    void save(Miembro miembro);
    void update(Miembro miembro);
    void remove(Miembro miembro);

}
