package com.richardoruna.app.adminchurch.presenter;

import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.model.remote.Constant;

import java.util.List;

/**
 * Created by Ricoru on 21/09/17.
 */

public interface ICelulaMemberPresenter {

    void getCelulaMembers();
    void data(List<Miembro> miembroList);

    void updateMiembroCelula(Celula celula);

    void success(Constant.typeAction typeAction);
    void error(Throwable t);

}
