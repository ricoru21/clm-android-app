package com.richardoruna.app.adminchurch.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.presenter.ILoginPresenter;
import com.richardoruna.app.adminchurch.presenter.LoginPresenter;
import com.richardoruna.app.adminchurch.view.ILoginView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements ILoginView {

    public static final String EXTRA_IS_INVITADO = "is_invitado";
    public static final int RC_SIGN_IN = 1;

    @BindView(R.id.img_logo_principal)
    ImageView img_logo_principal;
    @BindView(R.id.btnFacebook)
    LoginButton btnFacebook;

    View customeView_RestorePassword;

    ILoginPresenter iLoginPresenter;
    CallbackManager callbackManager;
    GoogleApiClient mGoogleApiClient;
    MaterialDialog progressDialog, dialogRestorePassword;

    public boolean is_invitado=true;

    public static void activityFrom(Context context, Bundle extra){
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtras(extra);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        iLoginPresenter = new LoginPresenter(this);

        //verificarHashCode();
        init();
        initFacebook();
        initGoogle();
        initBackground();
        createProgressDialog();
    }

    @Override
    protected void onStart() {
        super.onStart();
        iLoginPresenter.isUserLogin();
    }

    private void init(){
        if(getIntent().getExtras()!=null){
            is_invitado = getIntent().getBooleanExtra(EXTRA_IS_INVITADO,true);
        }
    }

    private void initBackground(){
        /*mediaUtil = new MediaUtil(this);
        Point point = mediaUtil.getDisplay();
        imgBackground.setBackground(new BitmapDrawable(getResources(),mediaUtil.decodeSampledBitmapFromResource(getResources(),
                R.drawable.custom_background_login_7, mediaUtil.dpToPx(point.x), mediaUtil.dpToPx(point.y))));*/
        Glide.with(LoginActivity.this)
                .load(R.drawable.logo_ac_)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(400, 400) {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        img_logo_principal.setImageBitmap(bitmap);
                    }
                });
    }

    private void initFacebook(){
        callbackManager = CallbackManager.Factory.create();
        btnFacebook.setReadPermissions(Arrays.asList("email","public_profile"));
        btnFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                iLoginPresenter.loginFacebook(accessToken.getToken());
            }

            @Override
            public void onCancel() {
                //si se cancela aquí hay q limpiar la sessión
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException exception) {
                iLoginPresenter.exception(exception);
            }
        });

    }

    private void initGoogle(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener(){
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        iLoginPresenter.error(R.string.error_connection_google);
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
    }

    private void createProgressDialog(){
        progressDialog = new MaterialDialog.Builder(this)
                .title(R.string.login_title_dialog)
                .content(R.string.all_msj_dialog)
                .progress(true, 5000)
                .widgetColorRes(R.color.colorD929)
                .canceledOnTouchOutside(false)
                .cancelable(false)
                .build();
    }

    @OnClick(R.id.btnGoogle)
    public void SignInGoogle(){
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @OnClick(R.id.btn_facebook)
    public void loginFacebook(){
        btnFacebook.performClick();
    }

    @OnClick(R.id.btn_google)
    public void loginGoogle(){
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @OnClick(R.id.txtRecuperarClave)
    public void restablecerClave(){
        boolean wrapInScrollView = true;
        dialogRestorePassword = new MaterialDialog.Builder(this)
                .title(R.string.lbl_restore_password)
                .positiveText(R.string.action_send)
                .negativeText(R.string.action_cancel)
                .positiveColorRes(R.color.colorPrimaryDark)
                .canceledOnTouchOutside(false)
                .cancelable(false)
                .customView(R.layout.custom_view_restore_password, wrapInScrollView)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        try {
                            AppCompatEditText email =(AppCompatEditText)customeView_RestorePassword.findViewById(R.id.edit_email);
                            dialogRestorePassword.dismiss();
                            iLoginPresenter.restorePassword(email.getText().toString());
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        dialogRestorePassword.dismiss();
                    }
                })
                .build();
        customeView_RestorePassword = dialogRestorePassword.getCustomView();
        dialogRestorePassword.show();
    }

    @Override
    public void message(@StringRes int msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void message(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void irRegisterAccount(Bundle extra) {
        if(extra==null) return;
        Intent intent = new Intent(LoginActivity.this, RegistroActivity.class);
        extra.putBoolean(RegistroActivity.INTENT_EXTRA_IS_INVITADO, is_invitado);
        intent.putExtras(extra);
        startActivity(intent);
    }

    @Override
    public void irHome() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void irUjier() {
        startActivity(new Intent(this, AsistenciaDominicalActivity.class));
    }

    @Override
    public void showProgress(@StringRes int title) {
        progressDialog.setTitle(title);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void clearLogginManager() {
        LoginManager.getInstance().logOut();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            callbackManager.onActivityResult(requestCode, resultCode, data);
            if (requestCode == RC_SIGN_IN) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                if (result.isSuccess()) {
                    // Google Sign In was successful, authenticate with Firebase
                    GoogleSignInAccount account = result.getSignInAccount();
                    iLoginPresenter.loginGoogle(account);
                } else {
                    iLoginPresenter.error(R.string.error_auth_failed_gooogle);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void verificarHashCode(){
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hash= Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.i("INFO","-hascode facebook wando :"+hash);
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("test-login-facebook","error al obtener keyHash");
        } catch (NoSuchAlgorithmException e) {
            Log.e("test-login-facebook","error en algoritmo hash");
        }
    }

}