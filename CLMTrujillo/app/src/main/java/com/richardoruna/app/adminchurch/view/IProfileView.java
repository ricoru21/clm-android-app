package com.richardoruna.app.adminchurch.view;

import com.richardoruna.app.adminchurch.model.entity.Usuario;

/**
 * Created by Ricoru on 17/09/17.
 */

public interface IProfileView extends IBaseView {

    void setData(Usuario usuario);
    void showProgress();
    void hideProgress();
    void finish();

}
