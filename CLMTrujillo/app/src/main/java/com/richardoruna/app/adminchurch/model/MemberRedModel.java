package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.model.dao.MemberDAO;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.presenter.IMemberRedPresenter;

import java.util.List;

/**
 * Created by Ricoru on 21/09/17.
 */

public class MemberRedModel implements IMemberRedModel {

    MemberDAO memberDAO;
    IMemberRedPresenter iMemberRedPresenter;

    public MemberRedModel(IMemberRedPresenter iMemberRedPresenter) {
        this.memberDAO = new MemberDAO();
        this.iMemberRedPresenter = iMemberRedPresenter;
    }

    @Override
    public void getMember(String red) {
        memberDAO.getMembers(red, new IComplete<List<Miembro>>() {
            @Override
            public void success(List<Miembro> objeto) {
                iMemberRedPresenter.data(objeto);
            }

            @Override
            public void failure(Throwable e) {
                iMemberRedPresenter.error(e);
            }
        });
    }

    @Override
    public void save(Miembro miembro) {
        memberDAO.save(miembro, new IComplete<Void>() {
            @Override
            public void success(Void objeto) {
                iMemberRedPresenter.success(Constant.typeAction.SAVE);
            }

            @Override
            public void failure(Throwable e) {
                iMemberRedPresenter.error(e);
            }
        });
    }

    @Override
    public void update(Miembro miembro){
        memberDAO.update(miembro, new IComplete<Void>() {
            @Override
            public void success(Void objeto) {
                iMemberRedPresenter.success(Constant.typeAction.UPDATE);
            }

            @Override
            public void failure(Throwable e) {
                iMemberRedPresenter.error(e);
            }
        });
    }

    @Override
    public void remove(Miembro miembro) {
        memberDAO.remove(miembro, new IComplete<Void>() {
            @Override
            public void success(Void objeto) {
                iMemberRedPresenter.success(Constant.typeAction.DELETE);
            }
            @Override
            public void failure(Throwable e) {
                iMemberRedPresenter.error(e);
            }
        });
    }

}
