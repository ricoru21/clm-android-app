package com.richardoruna.app.adminchurch.model;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.AuthResult;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.contract.IComplete;
import com.richardoruna.app.adminchurch.contract.ICompleteAuth;
import com.richardoruna.app.adminchurch.model.dao.AuthDAO;
import com.richardoruna.app.adminchurch.model.dao.UserDAO;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;
import com.richardoruna.app.adminchurch.presenter.ILoginPresenter;


/**
 * Created by Ricoru on 11/07/17.
 */

public class LoginModel implements ILoginModel {
    private static final String TAG = LoginModel.class.getSimpleName();

    AuthDAO authModel;
    UserDAO userDAO;
    ILoginPresenter iLoginPresenter;

    public LoginModel(ILoginPresenter iLoginPresenter) {
        authModel = AuthDAO.getInstance();
        userDAO = new UserDAO();
        this.iLoginPresenter = iLoginPresenter;
    }

    @Override
    public void loginFacebook(String token) {
        authModel.loginWithFacebook(token, new ICompleteAuth<AuthResult>() {
            @Override
            public void success(final AuthResult authResult) {
                if(authResult!=null){
                    userDAO.getProfileLiderSingle(authResult.getUser().getUid(), new IComplete<Usuario>() {
                        @Override
                        public void success(Usuario objeto) {
                            iLoginPresenter.success(authResult.getUser(), objeto);
                        }
                        @Override
                        public void failure(Throwable throwable) {
                            if (throwable == null )  iLoginPresenter.successNotExists(authResult.getUser(), FirebaseConstant.TypeAcount.Facebook); //no existe
                            else iLoginPresenter.exception( throwable); // hubo un error
                        }
                    });
                }
            }
            @Override
            public void failure(Throwable throwable) {
                LoginManager.getInstance().logOut();
                iLoginPresenter.exception(throwable);
            }
        });
    }

    @Override
    public void loginGoogle(GoogleSignInAccount account) {
        authModel.loginWithGoogle(account, new ICompleteAuth<AuthResult>() {
            @Override
            public void success(final AuthResult authResult) {
                if(authResult!=null){
                    userDAO.getProfileLiderSingle(authResult.getUser().getUid(), new IComplete<Usuario>() {
                        @Override
                        public void success(Usuario objeto) {
                            iLoginPresenter.success(authResult.getUser(), objeto);
                        }
                        @Override
                        public void failure(Throwable throwable) {
                            if (throwable == null )  iLoginPresenter.successNotExists(authResult.getUser(), FirebaseConstant.TypeAcount.Google); //no existe
                            else iLoginPresenter.exception( throwable); // hubo un error
                        }
                    });
                }
            }
            @Override
            public void failure(Throwable throwable) {
                iLoginPresenter.exception(throwable);
            }
        });
    }

    @Override
    public void restorePassword(String email) {
        authModel.restorePassword(email, new IComplete<Void>() {
            @Override
            public void success(Void objeto) {
                if(objeto==null){//ok
                    iLoginPresenter.successRestorePassword();
                }
            }
            @Override
            public void failure(Throwable throwable) {
                if(throwable!=null){//is error
                    throwable.printStackTrace();
                    iLoginPresenter.exception(throwable);
                }else{
                    iLoginPresenter.error(R.string.error_send_restore);
                }
            }
        });
    }


}
