package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.model.entity.AsistenciaCelula;

/**
 * Created by Ricoru on 21/09/17.
 */

public interface ICelulaAsistenciaRedModel extends ICelulaBaseModel {

    void save(AsistenciaCelula asistenciaCelula);
    void getAsistencias(String red);

}
