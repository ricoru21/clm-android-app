package com.richardoruna.app.adminchurch.presenter.asistencia;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.CelulaAsistenciaRedModel;
import com.richardoruna.app.adminchurch.model.ICelulaAsistenciaRedModel;
import com.richardoruna.app.adminchurch.model.entity.AsistenciaCelula;
import com.richardoruna.app.adminchurch.model.entity.Celula;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.ui.MvpApp;
import com.richardoruna.app.adminchurch.util.DateUtil;
import com.richardoruna.app.adminchurch.util.Preferences;
import com.richardoruna.app.adminchurch.view.ICelulaAsistenciaRedView;

import java.util.List;

/**
 * Created by Ricoru on 21/09/17.
 */

public class CelulaAsistenciaRedPresenter implements ICelulaAsistenciaRedPresenter {

    SharedPreferences utilPreferences;
    ICelulaAsistenciaRedView iCelulaAsistenciaRedView;
    ICelulaAsistenciaRedModel iCelulaAsistenciaRedModel;

    private Usuario usuario;
    public CelulaAsistenciaRedPresenter(ICelulaAsistenciaRedView iCelulaAsistenciaRedView) {
        this.iCelulaAsistenciaRedView = iCelulaAsistenciaRedView;
        this.iCelulaAsistenciaRedModel = new CelulaAsistenciaRedModel(this,null);
        utilPreferences = Preferences.getDefaultPreferences(MvpApp.getContext());
        usuario = new Gson().fromJson(
                Preferences.findStringPreference(utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY,""),
                Usuario.class);
    }

    @Override
    public void getCelulas() {
        iCelulaAsistenciaRedModel.getCelula(usuario.red);
    }

    @Override
    public void data(List<Celula> celulaList) {
        iCelulaAsistenciaRedView.dataOptionSpinner(celulaList);
    }

    @Override
    public void save(String fechaAsistencia, Celula celula) {
        AsistenciaCelula asistenciaCelula = new AsistenciaCelula();
        asistenciaCelula.celula = celula;
        asistenciaCelula.nroMiembros = celula.miembroList.size();
        int cont_asistio=0;
        for (Miembro miembro: celula.miembroList) {
            if(miembro.asistio){
                cont_asistio++;
            }
        }
        asistenciaCelula.nroAsistido = cont_asistio;
        asistenciaCelula.fechaAsistencia = String.valueOf(DateUtil.formatStringToDate(fechaAsistencia));
        asistenciaCelula.fechaRegistro = String.valueOf(DateUtil.getDateHour());
        iCelulaAsistenciaRedView.showProgress();
        iCelulaAsistenciaRedModel.save(asistenciaCelula);
    }

    @Override
    public void error(Throwable t) {
        if(t!=null) iCelulaAsistenciaRedView.message(t.getMessage());
        else iCelulaAsistenciaRedView.message(R.string.error_internal);
    }

    @Override
    public void success(Constant.typeAction typeAction) {
        if(typeAction.equals(Constant.typeAction.SAVE)){
            iCelulaAsistenciaRedView.message(R.string.celula_asistencia_sucess);
            iCelulaAsistenciaRedView.clearData();
            iCelulaAsistenciaRedView.hideProgress();
        }
    }

}
