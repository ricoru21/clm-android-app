package com.richardoruna.app.adminchurch.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.presenter.IProfilePresenter;
import com.richardoruna.app.adminchurch.presenter.ProfilePresenter;
import com.richardoruna.app.adminchurch.view.IProfileView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ricoru on 17/09/17.
 */

public class PerfilFragment extends Fragment implements IProfileView {

    @BindView(R.id.txt_name)
    TextView txt_name;
    @BindView(R.id.txt_email)
    TextView txt_email;
    @BindView(R.id.txt_red)
    TextView txt_red;
    @BindView(R.id.img_foto)
    CircleImageView img_foto;

    private Unbinder unbinder;
    private IProfilePresenter iProfilePresenter;

    public PerfilFragment() {
        // Required empty public constructor
    }

    public static PerfilFragment newInstance() {
        PerfilFragment fragment = new PerfilFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        iProfilePresenter = new ProfilePresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        iProfilePresenter.getData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Glide.with(this).onDestroy();
        unbinder.unbind();
    }

    @Override
    public void setData(Usuario usuario) {
        txt_name.setText(usuario.name);
        txt_email.setText(usuario.email);
        txt_red.setText(usuario.red);

        if(usuario.foto!=null){
            Glide.with(this)
                    .load(usuario.foto)
                    .override(100,100)
                    .fitCenter()
                    .crossFade()
                    .into(img_foto);
        }
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void finish() {

    }

    @OnClick(R.id.txt_historial_asistencia)
    public void irHistorialAsistencia(){
        iProfilePresenter.irHistorialAsistencia(getActivity());
    }

    @OnClick(R.id.txt_cerrar_sesion)
    public void cerrarSesion(){
        iProfilePresenter.clearPreferences();
        getActivity().finish();
    }

    @OnClick(R.id.btn_edit_perfil)
    public void editarPerfil(){
        iProfilePresenter.irEditarPerfil(getActivity());
    }

    @Override
    public void message(@StringRes int msg) {
        Toast.makeText(getActivity(),msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void message(String msg) {
        Toast.makeText(getActivity(),msg, Toast.LENGTH_SHORT).show();
    }

}
