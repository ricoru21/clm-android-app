package com.richardoruna.app.adminchurch.presenter.asistencia;

import android.content.Context;

import com.richardoruna.app.adminchurch.model.entity.AsistenciaDominical;
import com.richardoruna.app.adminchurch.model.entity.Turno;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.presenter.IPreferencePresenter;

import java.util.List;

/**
 * Created by Ricoru on 22/09/17.
 */

public interface IAsistenciaDominicalPresenter extends IPreferencePresenter {

    void getAsistencias();
    void data(List<AsistenciaDominical> asistenciaDominicalList);

    void getTurnos();
    void dataTurnos(List<Turno> turnoList);
    void error(Throwable e);

    void save(Turno turno, String fecha_asistencia, String fecha_asistencia_format,String nh_adultos,
              String nh_jovenes, String nh_ninios, String nm_adultas, String nm_jovenes, String n_nuevos);

    void success(Constant.typeAction typeAction);
    void irRegistroAsistencia(Context context);
    void irEditarAsistencia(Context context);

}
