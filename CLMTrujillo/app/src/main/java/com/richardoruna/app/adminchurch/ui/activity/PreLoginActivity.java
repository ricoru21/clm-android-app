package com.richardoruna.app.adminchurch.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.presenter.IPreLoginPresenter;
import com.richardoruna.app.adminchurch.presenter.PreLoginPresenter;
import com.richardoruna.app.adminchurch.view.IPreLoginView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.richardoruna.app.adminchurch.ui.activity.LoginActivity.EXTRA_IS_INVITADO;

public class PreLoginActivity extends AppCompatActivity implements IPreLoginView {

    @BindView(R.id.img_logo_principal)
    ImageView img_logo_principal;

    IPreLoginPresenter iPreLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_login);
        ButterKnife.bind(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        init();
        initBackground();
    }

    private void init(){
        iPreLoginPresenter = new PreLoginPresenter(this);
    }

    private void initBackground(){
        /*mediaUtil = new MediaUtil(this);
        Point point = mediaUtil.getDisplay();
        imgBackground.setBackground(new BitmapDrawable(getResources(),mediaUtil.decodeSampledBitmapFromResource(getResources(),
                R.drawable.custom_background_login_7, mediaUtil.dpToPx(point.x), mediaUtil.dpToPx(point.y))));*/
        Glide.with(PreLoginActivity.this)
                .load(R.drawable.logo_ac_)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(400, 400) {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        img_logo_principal.setImageBitmap(bitmap);
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        iPreLoginPresenter.isUserLogin();
    }

    @OnClick(R.id.btn_invitado)
    public void ingresarInvitado(){
        Toast.makeText(this, "Está opción no esta disponible", Toast.LENGTH_SHORT).show();
        /*Bundle bundle = new Bundle();
        bundle.putBoolean(EXTRA_IS_INVITADO, true);
        LoginActivity.activityFrom(this, bundle);*/
    }

    @OnClick(R.id.btn_responsable)
    public void ingresarResponsable(){
        Bundle bundle = new Bundle();
        bundle.putBoolean(EXTRA_IS_INVITADO, false);
        LoginActivity.activityFrom(this, bundle);
    }

    @Override
    public void message(@StringRes int msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void message(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void irHome() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void irUjier() {
        startActivity(new Intent(this, AsistenciaDominicalActivity.class));
    }

}
