package com.richardoruna.app.adminchurch.presenter;

import android.support.annotation.StringRes;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseUser;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;


/**
 * Created by Ricoru on 11/07/17.
 */

public interface ILoginPresenter<T> {


    void loginFacebook(String token);
    void loginGoogle(GoogleSignInAccount account);
    void restorePassword(String email);

    void successRestorePassword();
    void success(FirebaseUser fbuser, Usuario usuario);
    void successNotExists(FirebaseUser fbuser, FirebaseConstant.TypeAcount typeAcount);
    void exception(Throwable e);
    void error(@StringRes int msj);

    void validarRoles();
    void isUserLogin();
    boolean isSlider();

}
