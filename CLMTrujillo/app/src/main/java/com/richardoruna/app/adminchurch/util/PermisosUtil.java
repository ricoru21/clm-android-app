package com.richardoruna.app.adminchurch.util;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import com.richardoruna.app.adminchurch.ui.MvpApp;

/**
 * Created by Ricoru on 12/07/17.
 */

public class PermisosUtil {

    public static final int REQUEST_CODE_READ_PERMISSION = 22;

    public static boolean hasGalleryPermission() {
        return ActivityCompat.checkSelfPermission(MvpApp.getContext(),
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static void askForGalleryPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                REQUEST_CODE_READ_PERMISSION);
    }

}
