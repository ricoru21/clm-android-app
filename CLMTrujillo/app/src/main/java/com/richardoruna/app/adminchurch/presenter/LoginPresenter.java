package com.richardoruna.app.adminchurch.presenter;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.StringRes;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.ILoginModel;
import com.richardoruna.app.adminchurch.model.LoginModel;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;
import com.richardoruna.app.adminchurch.ui.MvpApp;
import com.richardoruna.app.adminchurch.util.Preferences;
import com.richardoruna.app.adminchurch.view.ILoginView;

import static com.richardoruna.app.adminchurch.ui.activity.RegistroActivity.INTENT_EXTRA_EMAIl;
import static com.richardoruna.app.adminchurch.ui.activity.RegistroActivity.INTENT_EXTRA_NAME;
import static com.richardoruna.app.adminchurch.ui.activity.RegistroActivity.INTENT_EXTRA_TYPE_ACCOUNT;
import static com.richardoruna.app.adminchurch.ui.activity.RegistroActivity.INTENT_EXTRA_UID;


/**
 * Created by Ricoru on 11/07/17.
 */

public class LoginPresenter implements ILoginPresenter {
    private static final String TAG = LoginPresenter.class.getSimpleName();

    private ILoginView view;
    private ILoginModel loginModel;
    private SharedPreferences utilPreferences;

    Usuario usuario;
    String rol_seleccionado = "";

    public LoginPresenter(ILoginView view){
        this.view = view;
        loginModel = new LoginModel(this);
        utilPreferences = Preferences.getDefaultPreferences(MvpApp.getContext());
    }

    @Override
    public void loginFacebook(String token) {
        view.showProgress(R.string.login_title_dialog);
        loginModel.loginFacebook(token);
    }

    @Override
    public void loginGoogle(GoogleSignInAccount account) {
        view.showProgress(R.string.login_title_dialog);
        loginModel.loginGoogle(account);
    }

    @Override
    public void restorePassword(String email) {
        view.showProgress(R.string.login_restore_title_dialog);
        loginModel.restorePassword(email);
    }

    @Override
    public void successRestorePassword() {
        view.hideProgress();
        view.message(R.string.success_send_restore);
    }

    @Override
    public void success(FirebaseUser fbuser, Usuario usuario) {
        usuario.foto = fbuser.getPhotoUrl().toString();
        this.usuario = usuario;
        while(this.usuario.roles.remove(null));
        //remover nulls
        /*Iterator<String> itr= usuario.roles.iterator();
        while(itr.hasNext()){
            if(itr.next() == null){
                itr.remove();
            }
        }*/
        //hacer proceso de login oh guardar el uid;
        Preferences.storeStringPreference(
                utilPreferences, Preferences.PREFERENCES_USER_UID, fbuser.getUid());
        Preferences.storeStringPreference(
                utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY, new Gson().toJson(usuario));
        if(usuario.roles!=null){
            for (String position: usuario.roles) {
                if(position!=null){
                    rol_seleccionado = position;
                    break;
                }
            }
            Preferences.storeStringPreference(
                    utilPreferences, Preferences.PREFERENCES_USER_ROL_SELECT, rol_seleccionado);
        }
        view.clearLogginManager();
        view.hideProgress();
        validarRoles();
    }

    @Override
    public void successNotExists(FirebaseUser fbuser, FirebaseConstant.TypeAcount typeAcount) {
        Bundle extra = new Bundle();
        extra.putString(INTENT_EXTRA_UID, fbuser.getUid());
        extra.putString(INTENT_EXTRA_EMAIl, fbuser.getEmail());
        extra.putString(INTENT_EXTRA_NAME, fbuser.getDisplayName());
        extra.putString(INTENT_EXTRA_TYPE_ACCOUNT, typeAcount.name());
        view.hideProgress();
        view.irRegisterAccount(extra);
    }

    @Override
    public void exception(Throwable e) {
        e.printStackTrace();
        view.clearLogginManager();
        view.message(e.getMessage());
        view.hideProgress();
    }

    @Override
    public void error(@StringRes int msj) {
        view.message(msj);
        view.hideProgress();
    }

    @Override
    public void validarRoles() {
        try{
            if( usuario.estado.equalsIgnoreCase(Constant.TAG_ESTADO_HABILITADO)) {
                if (rol_seleccionado.equalsIgnoreCase(Constant.roles.get(0))) { //invitado
                    view.message("No está disponible el ingreso como invitado");
                } else if (rol_seleccionado.equalsIgnoreCase(Constant.roles.get(1))) { //Es Lider o Pastor
                    view.irHome();
                    view.finish();
                } else if (rol_seleccionado.equalsIgnoreCase(Constant.roles.get(2))) { //Es Lider o Pastor
                    view.irHome();
                    view.finish();
                } else if (rol_seleccionado.equalsIgnoreCase(Constant.roles.get(3))) { //Es Ujier
                    view.irUjier();
                    view.finish();
                }
            } else if( usuario.estado.equalsIgnoreCase( Constant.TAG_ESTADO_IN_HABILITADO)){
                view.message("Su cuenta se encuentra inhabilitada, comuniquese con el Administrador.");
            } else if( usuario.estado.equalsIgnoreCase( Constant.TAG_ESTADO_POR_APROBAR)){
                view.message("Su cuenta necesita ser aprobada, intentelo más tarde");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void isUserLogin() {
        String uid = Preferences.findStringPreference(utilPreferences, Preferences.PREFERENCES_USER_UID, "");
        //verificar el Perfil
        if(!uid.equalsIgnoreCase("")){
            usuario = new Gson().fromJson(
                    Preferences.findStringPreference(utilPreferences,
                            Preferences.PREFERENCES_USER_TEMPORARY, ""), Usuario.class);
            rol_seleccionado = Preferences.findStringPreference(
                    utilPreferences, Preferences.PREFERENCES_USER_ROL_SELECT, "");
            validarRoles();
        }
    }

    @Override
    public boolean isSlider() {
        return Preferences.findBooleanPreference(utilPreferences, Preferences.PREFERENCES_FIRST_PROPERTY_SLIDE, true);
    }

}
