package com.richardoruna.app.adminchurch.presenter;

import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.model.IMemberRedModel;
import com.richardoruna.app.adminchurch.model.MemberRedModel;
import com.richardoruna.app.adminchurch.model.entity.Usuario;
import com.richardoruna.app.adminchurch.model.entity.Miembro;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.ui.MvpApp;
import com.richardoruna.app.adminchurch.util.Preferences;
import com.richardoruna.app.adminchurch.view.IRedMemberView;

import java.util.Collections;
import java.util.List;

/**
 * Created by Ricoru on 21/09/17.
 */

public class MemberRedPresenter implements IMemberRedPresenter {

    IRedMemberView iRedMemberView;
    IMemberRedModel iMemberRedModel;
    SharedPreferences utilPreferences;

    private Usuario usuario;
    private List<Miembro> miembroList;

    public MemberRedPresenter(IRedMemberView iRedMemberView) {
        this.iRedMemberView = iRedMemberView;
        this.iMemberRedModel = new MemberRedModel(this);
        utilPreferences = Preferences.getDefaultPreferences(MvpApp.getContext());
        usuario = new Gson().fromJson(
                Preferences.findStringPreference(utilPreferences, Preferences.PREFERENCES_USER_TEMPORARY,""),
                Usuario.class);

    }

    @Override
    public void getMembers() {
        iMemberRedModel.getMember(usuario.red);
    }

    @Override
    public void data(List<Miembro> miembroList) {
        this.miembroList = miembroList;
        Collections.reverse(miembroList);
        iRedMemberView.setModel(miembroList);
    }

    @Override
    public void save(String nombre, String telefono_celular, String email) {
        if(TextUtils.isEmpty(nombre)){
            iRedMemberView.message("* El campo nombre es requerido *");
            return;
        }
        Miembro miembro = new Miembro();
        miembro.email= email;
        miembro.celular = telefono_celular;
        miembro.name = nombre;
        miembro.red = usuario.red;
        iMemberRedModel.save(miembro);
    }

    @Override
    public void update(int position, String nombre, String telefono_celular, String email) {
        if(TextUtils.isEmpty(nombre)){
            iRedMemberView.message("* El campo nombre es requerido *");
            return;
        }
        Miembro miembro = miembroList.get(position);
        miembro.email= email;
        miembro.name = nombre;
        miembro.celular = telefono_celular;
        iRedMemberView.showProgress();
        iMemberRedModel.update(miembro);
    }

    @Override
    public void remove(int position) {
        iRedMemberView.showProgress();
        iMemberRedModel.remove(miembroList.get(position));
    }

    @Override
    public void success(Constant.typeAction typeAction) {
        iRedMemberView.hideProgress();
        if(typeAction.equals(Constant.typeAction.SAVE)){
            iRedMemberView.message(R.string.success_reg_member);
        }else if(typeAction.equals(Constant.typeAction.UPDATE)){
            iRedMemberView.message(R.string.success_upd_member);
        }else if(typeAction.equals(Constant.typeAction.DELETE)){
            iRedMemberView.message(R.string.success_del_member);
        }
    }

    @Override
    public void error(Throwable e) {
        iRedMemberView.hideProgress();
        if(e!=null) iRedMemberView.message(e.getMessage());
        else iRedMemberView.message(R.string.error_internal);
    }

}
