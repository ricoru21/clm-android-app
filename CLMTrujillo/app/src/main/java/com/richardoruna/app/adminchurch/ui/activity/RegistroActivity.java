package com.richardoruna.app.adminchurch.ui.activity;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.richardoruna.app.adminchurch.model.remote.Constant;
import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;
import com.richardoruna.app.adminchurch.presenter.IRegisterPresenter;
import com.richardoruna.app.adminchurch.presenter.RegisterPresenter;
import com.richardoruna.app.adminchurch.view.IRegisterView;
import com.richardoruna.app.adminchurch.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistroActivity extends AppCompatActivity implements IRegisterView {

    public static String INTENT_EXTRA_UID = "firebase_uid";
    public static String INTENT_EXTRA_EMAIl= "firebase_email";
    public static String INTENT_EXTRA_NAME = "firebase_name";
    public static String INTENT_EXTRA_TYPE_ACCOUNT = "typr_account";
    public static String INTENT_EXTRA_IS_INVITADO = "is_invitado";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edit_name)
    AppCompatEditText edit_name;
    @BindView(R.id.edit_email)
    AppCompatEditText edit_email;
    @BindView(R.id.sp_redes)
    Spinner sp_redes;

    String type_account="", firebase_uid="";
    boolean is_invitado = true;
    ActionBar actionBar;
    IRegisterPresenter iRegisterPresenter;
    MaterialDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        iRegisterPresenter = new RegisterPresenter(this);

        initToolbar();
        createProgressDialog();
        init();
    }

    private void init(){
        if( getIntent().getExtras()!=null ){
            try{
                edit_name.setText(getIntent().getExtras().getString(INTENT_EXTRA_NAME,""));
                edit_email.setText(getIntent().getExtras().getString(INTENT_EXTRA_EMAIl,""));
                firebase_uid = getIntent().getExtras().getString(INTENT_EXTRA_UID,"");
                type_account = getIntent().getExtras().getString(INTENT_EXTRA_TYPE_ACCOUNT,"");
                is_invitado = getIntent().getExtras().getBoolean(INTENT_EXTRA_IS_INVITADO);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        //ActionBar actionBar = getSupportActionBar();
        actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back);
    }

    private void createProgressDialog(){
        progressDialog = new MaterialDialog.Builder(this)
                .title(R.string.register_title_dialog)
                .content(R.string.all_msj_dialog)
                .progress(true, 5000)
                .widgetColorRes(R.color.colorD929)
                .build();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            iRegisterPresenter.clearLoginFacebook();
            finish();
            return true;
        }
        return false;
    }

    @OnClick(R.id.btn_save)
    public void save(){
        FirebaseConstant.TypeAcount acount = null;
        switch (type_account){
            case "Facebook":
                acount = FirebaseConstant.TypeAcount.Facebook;
                break;
            case "Google":
                acount = FirebaseConstant.TypeAcount.Google;
                break;
        }

        String estado="", rol ="";
        if(is_invitado){
            estado = Constant.TAG_ESTADO_HABILITADO;
        }else{
            estado = Constant.TAG_ESTADO_POR_APROBAR;
        }
        iRegisterPresenter.validate(
                firebase_uid, edit_name.getText().toString(),
                edit_email.getText().toString(), sp_redes.getSelectedItem().toString(), acount,
                estado);
    }

    @Override
    public void message(@StringRes int msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void message(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

}
