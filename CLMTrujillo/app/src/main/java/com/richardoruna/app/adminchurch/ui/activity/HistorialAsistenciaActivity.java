package com.richardoruna.app.adminchurch.ui.activity;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

import com.richardoruna.app.adminchurch.R;
import com.richardoruna.app.adminchurch.adapter.HistorialAsistenciaAdapter;
import com.richardoruna.app.adminchurch.model.entity.AsistenciaCelula;
import com.richardoruna.app.adminchurch.presenter.asistencia.HistorialAsistenciaPresenter;
import com.richardoruna.app.adminchurch.presenter.asistencia.IHistorialAsistenciaPresenter;
import com.richardoruna.app.adminchurch.ui.common.SimpleDividerItemDecoration;
import com.richardoruna.app.adminchurch.view.IHistorialAsistenciaView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistorialAsistenciaActivity extends AppCompatActivity implements IHistorialAsistenciaView {

    @BindView(R.id.rcv_lista)
    RecyclerView rcv_lista;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_principal)
    LinearLayout content_principal;

    ActionBar actionBar;
    IHistorialAsistenciaPresenter iHistorialAsistenciaPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_asistencia);
        ButterKnife.bind(this);
        iHistorialAsistenciaPresenter = new HistorialAsistenciaPresenter(this);

        init();
        initToolbar();
    }

    private void init(){
        iHistorialAsistenciaPresenter.getAsistencias();
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        //ActionBar actionBar = getSupportActionBar();
        actionBar = getSupportActionBar();
        actionBar.setTitle("");
        //actionBar.setDisplayHomeAsUpEnabled(true);
        //toolbar.setNavigationIcon(R.mipmap.ic_arrow_back);
    }

    @Override
    public void setModel(List<AsistenciaCelula> data) {
        try {
            if (rcv_lista != null) {
                HistorialAsistenciaAdapter adapter = new HistorialAsistenciaAdapter(data);
                rcv_lista.setAdapter(adapter);
                rcv_lista.setLayoutManager(new LinearLayoutManager(this));
                rcv_lista.setHasFixedSize(true);
                rcv_lista.addItemDecoration(new SimpleDividerItemDecoration(this)
                );
                //rcv_lista.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void message(@StringRes int msj) {
        Snackbar.make(content_principal, msj, Snackbar.LENGTH_SHORT)
                .setAction("Action", null)
                .show();
    }

    @Override
    public void message(String msj) {
        Snackbar.make(content_principal, msj, Snackbar.LENGTH_SHORT)
                .setAction("Action", null)
                .show();
    }

}
