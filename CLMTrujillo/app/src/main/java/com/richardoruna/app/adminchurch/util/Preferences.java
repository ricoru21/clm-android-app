package com.richardoruna.app.adminchurch.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.richardoruna.app.adminchurch.BuildConfig;

/**
 * Created by ricoru on 28/06/16.
 */
public class Preferences {

    public static final String PREFERENCES_NAME = BuildConfig.APPLICATION_ID;
    public static final String PREFERENCES_USER_UID = "user_uid";
    public static final String PREFERENCES_USER_TEMPORARY = "user_temporary";
    public static final String PREFERENCES_USER_ROL_SELECT = "user_rol_select";
    public static final String PREFERENCES_FIRST_PROPERTY_SLIDE = "first_property_slide";

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }
    public static SharedPreferences getDefaultPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
    public static void storeBooleanPreference(SharedPreferences sharedPreferences, String name, boolean value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(name, value);
        editor.apply();
    }
    public static void storeStringPreference(SharedPreferences sharedPreferences, String name, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(name, value);
        editor.apply();
    }
    public static void storeIntPreference(SharedPreferences sharedPreferences, String name, int value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(name, value);
        editor.apply();
    }
    public static boolean findBooleanPreference(SharedPreferences sharedPreferences, String name, boolean _default) {
        return sharedPreferences.getBoolean(name, _default);
    }
    public static String findStringPreference(SharedPreferences sharedPreferences, String name, String _default) {
        return sharedPreferences.getString(name, _default);
    }
    public static int findIntPreference(SharedPreferences sharedPreferences, String name, int _default) {
        return sharedPreferences.getInt(name, _default);
    }
    public static void removePreference(SharedPreferences sharedPreferences, String name){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(name);
        editor.apply();
    }

}