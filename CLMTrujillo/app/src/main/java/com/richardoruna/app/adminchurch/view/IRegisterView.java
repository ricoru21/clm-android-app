package com.richardoruna.app.adminchurch.view;

/**
 * Created by Ricoru on 11/07/17.
 */

public interface IRegisterView extends MvpView {
    void showProgress();
    void hideProgress();
    void finish();
}
