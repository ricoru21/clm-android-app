package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.model.entity.Usuario;

/**
 * Created by Ricoru on 18/09/17.
 */

public interface IProfileModel {
    void updateProfile(Usuario usuario);
}
