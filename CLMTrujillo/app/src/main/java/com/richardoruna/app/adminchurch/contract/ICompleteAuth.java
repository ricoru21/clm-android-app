package com.richardoruna.app.adminchurch.contract;

/**
 * Created by Ricoru on 11/07/17.
 */

public interface ICompleteAuth<T> {
    void success(T objeto);
    void failure(Throwable throwable);
}
