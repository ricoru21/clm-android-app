package com.richardoruna.app.adminchurch.contract;

/**
 * Created by Ricoru on 11/07/17.
 */

public interface IComplete<T> {
    void success(T objeto);
    void failure(Throwable e);
}
