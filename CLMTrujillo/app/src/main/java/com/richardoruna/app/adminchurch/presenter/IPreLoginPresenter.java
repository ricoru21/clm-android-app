package com.richardoruna.app.adminchurch.presenter;

/**
 * Created by Ricoru on 24/09/17.
 */

public interface IPreLoginPresenter {

    void validarRoles();
    void isUserLogin();

}
