package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.model.remote.FirebaseConstant;

/**
 * Created by Ricoru on 11/07/17.
 */

public interface IRegisterModel {

    void saveRedSocial(String uid, String name, String email, String red, FirebaseConstant.TypeAcount typeAcount,
                       String estado);

}
