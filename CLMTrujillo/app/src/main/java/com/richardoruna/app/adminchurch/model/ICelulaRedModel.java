package com.richardoruna.app.adminchurch.model;

import com.richardoruna.app.adminchurch.model.entity.Celula;

/**
 * Created by Ricoru on 21/09/17.
 */

public interface ICelulaRedModel extends ICelulaBaseModel {

    void saveCelula(Celula celula);

}
